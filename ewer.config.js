const {
  asin,
  isbn,
  version,
  license,
  repository,
  description: title,
  authors,
} = require("./package.json");

module.exports = {
  meta: {
    version: "1.0.0",
    type: "ewer:ebook",
  },
  general: {
    bookTypesToUpload: ["rtf"],
  },
  metadata: {
    title,
    authors,
    outputProfile: "kindle",
    bookProducer: "ewer",
    comments: license,
    isbn,
    pubdate: "2020-07-22",
    publisher: "Michael Schmidt",
  },
  additionalMetadata: {
    asin,
    version,
    license,
    repository,
    rawBookUrl:
      "Place a url here or let it be fill automatically with uploaded books",
    stripHeadlinesAndFormatsFrom: ["rtf", "txt"],
  },
  lookAndFeel: {
    extraCss: `${__dirname}/media/global.css`,
    embedAllFonts: true,
    lineHeight: 22,
    changeJustification: "justify",
  },
  txtInputOptions: {
    formattingType: "markdown",
  },
  mobiOutputOptions: {
    mobiFileType: "both",
  },
  epubOutputOptions: {},
  azw3OutputOptions: {
    prettyPrint: true,
  },
  pdfOutputOptions: {
    customSizeCm: "12x19",
    customSize: null,
    paperSize: null,
    pdfPageNumbers: true,
    prettyPrint: true,
    pdfDefaultFontSize: 14.5,
    pdfAddToc: true,
    pdfHyphenate: true,
    pdfOddEvenOffset: 0,
    pdfPageMarginTop: 36.0,
    pdfPageMarginBottom: 36.0,
    pdfPageMarginLeft: 36.0,
    pdfPageMarginRight: 36.0,
  },
  txtOutputOptions: {
    forceMaxLineLength: null,
    inlineToc: false,
    keepColor: false,
    keepImageReferences: false,
    keepLinks: false,
    prettyPrint: true,
    txtOutputFormatting: "plain",
  },
  rtfOutputOptions: {
    prettyPrint: true,
  },
  tableOfContents: {
    tocTitle: "Inhalt",
    level1Toc: "//h:h2",
    // level2Toc: '//h:h2',
    // level3Toc: '//h:h5',
    useAutoToc: true,
  },
};
