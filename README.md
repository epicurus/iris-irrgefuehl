# Willkommen!

zum Heft 1 in der Reihe _Gemeinsam im Gespräch ... mit Iris Johansson_.
Ein Projekt von "Der Garten des Epikur".

[![Eine Frage zum Heft hinzufügen](https://img.youtube.com/vi/7l2YwmeOczU/0.jpg)](https://www.youtube.com/watch?v=7l2YwmeOczU "Eine Frage zum Heft hinzufügen")

## Gemeinsam im Gespräch ... über Irrgefühl

#### Im Alter von 12 Jahren fragte Iris ihren Vater, warum Menschen überhaupt streiten und nicht einfach liebevoll mteinander umgehen, sie zeigte auf ein Liebespärchen auf einer Parkbank. Das Leben ist doch so kurz. Und ihr Vater antwortete: "Das musst du ihnen beibringen." Und sie beschloss, alles über Kommunkation zu lernen, was es zu lernen gab, und tat es dann fast 60 Jahre, mit dem unbrechbaren Willen, den Autisten haben.

## Der Garten des Epikur

Epikur, der Philosoph der Freude im Leben, ist vielleicht der, der Iris Philosophie am nächsten ist.
Deswegen haben wir unser Projekt so genannt, das in der nächsten Zeit jede Menge Iris-Texte in die deutsche und englische Sprache bringen will und als gedruckte Hefte, E-Books und Audio-Bücher herausbringen will. Wir sind:

#### Heute erscheint dieser Entwicklungweg in ihren Antworten, Anregungen, einer Lebensphilosophie, die sie im Augenblick der Empfängnis beginnen lässt. Da wo jeder von uns der individuelle Mensch wird, der wir sind, da wo wir die ersten Tage als eine Zeit erleben, wo wir nichts von außen brauchen, in vollkommender Geborgenheit dem eigenen Werden folgen können. DAnn nehmen wir, nach etwa 14 Tagen Kontakt auf mit dem Körper unserer Mutter und beginnen zu kommunizieren. Wir lernen, dass wir nur sagen müssen, was wir brauchen, und das bekommen wir. So sind wir gemacht. So ist unser Anfang. Von allen Menschen. Die, bei denen es nicht so war, haben es nicht bis zur Geburt geschafft. Wir, die wir überlebt haben, haben das in uns. Unser Körper ist so gebaut.

#### Wenn wir dann später, nachdem wir geboren sind, Traumatisches erleben, Dinge, die wir nicht im Augenblick verarbeiten können oder uns entscheiden, es nicht zu tun, dann behält er das in seinem Gedächtnis. Er speichert es ein. Und dann, wenn die Zeit dafür gekommen, vielleicht viel später im Leben, vielleicht Jahrzehnte später, kommt der Moment, wo der Körper uns dieses Erlebnis in seiner ureigenen Art wiederbringt. Damit wir nochmal darauf schauen können. Und das tut er mit dem, was in diesem Buch Irrgefühl genannt wird, im Schwedischen, der Sprache von Iris, Ångest heißt. Ein bedrückendes Gefühl, das sich amöbenartig im Körper ausbreitet, uns lähmt: Kopfschmerzen, Unlust ... Alles, was ganz auf der anderen Seite steht von dem Leben mit offenen Armen und quicklebendig entgegenspringen, wie ein zwei-jähriges Kind, das gerade laufen gelernt hat. Das Gegenteil davon ... Und das ist liebevolle Kommunikation unseres eigenen Körper, nur für uns, ganz speziell, irgendetwas aus unserer Vergangenheit, was wir mitschleppen und auch wie los werden können ... Darüber geht dieses Heft.
