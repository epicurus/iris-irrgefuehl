## What is anxiety?

The most serious fear that we don't even dare thinking about, without this turning to anxiety immediately it is that which is called death anxiety.
When we are children we do not know what death means.
That it is the end of living here and now.
That we return to the same state that we were in before we came about.
We are split into the material and the immaterial.
The body returns to the soil.
And is turned over into soil there.
The immaterial that is without substance, it goes back to the dark in the universe in turns into information that all living can profit from.
During the development to becoming a grown up the ability ripens this fact really to comprehend and if we then of some reason have been in a situation where our life was threatened, then this memory can become clear as crystal.
And trigger fear, that really should need to unload, but unfortunately often this is being put away and the shock we got when this happened turns into trauma.
And this we will try to avoid coming close to.
Often all our live.

If we do not somewhere along the way get the courage to remain in that and anxiety, experience the pain, unload the fear until it can erase itself from the body and leave a chaos behind, that if we remain in it, it turns into an empty room.
But it's possible to take out the experience OF this hard thing and to get enriched with an experience of freedom, that is independent of outer things.
Then incompleteness becomes enriching.
In the very consciousness we get out of this experience, that we did with this specific moment.
The worst consequence of death anxiety is that we do not dare living life alive.
And we do not dare being playful and glad.
Because then it realises itself that we are mortal and that's the thing we try to avoid, in order not to feel the anxiety.
That restricts and put limits and boundaries to our life in a way that creates.
It doesn't matter instead of creating feelings it creates the kind of not moving.
And this makes the need for emotions bigger.
And is often diagnozed as depression.

What anxiety is that is a question that often arises in humans.
Most people have felt this feeling of something crawling along their backs.
Some kind of unease, it feels like the hair in our neck is getting up right.
It is felt like there is some kind of phantom in the darkness inside and sometimes outside as well.
That wants to cause you harm and that arttacks and that is life threatening.
Something that one wants to get rid of, get away from.
You would rather to extinguish it and to forget that it has ever felt like that.
A state of most pain.

<div class="handwritten">
Yes, that's how it feels.
And it can be felt thousands of other ways as well, even worse or less worse.
I don't get it why we humans have to live and to have this pain and this being trapped.
I can see no point in that.
</div>

Do you know what anxiety is?

<div class="handwritten">
How do you mean when you ask like that?
Anxiety is an anxiety, isn't it and every person has felt that, hasn't he or she.
</div>

Anxiety is an amoebic kind of feeling that arises when the body does not anymore remember, what is created this basic feelings for.
It creates feelings in order to make changes in our inner being and make these changes possible.
But if we do not live in accordance with that then these feelings are saved inside of the selves.
And when the body does not remember them any longer they turn into into anxiety.
What we call emotional reactions it is something that is that something happens that makes our body create these feelings.
For instance we lose something that is dare and valuable to us.
In order to stand this we need some kind of transit from having had something in a certain way until we lost the same thing.
In the first place the body loses it's mode.
The thoughts and the time stands still.
It gets unreal.
Then the reaction comes, we shake somehow and then we start crying we get mad, the fear being sad.
The body unloads something so that it's possible to think anew, and that this can happen.

If we just run over that then this unloading does not turn into that which it should have been.
But instead these feelings poison us within.
They remain as garbage in ourselves.
It can occur like waves over long period of time.
Or it can come as one single breakdown, so that we remain when unloading until we get empty inside.
This emptiness is a vacuum.
And at the same time is a chaos.
A kind of unclear state.
Nothing is like it used to be any more.
And it's not possible to go on as if this wouldn't have happened.
And still life goes on because of this emotional reaction.
After having unloaded there will be an empty room inside.
That is the remains of what has been and if we remain in that it will fill up from underneath from inside, from our own experience and it would breathe.
And in this way the possibility to think differently to think anew, to change arise.
Life goes on but it's sad that we have such an ideal.
And that it's negative to unload feelings and to change oneself.

In accordance with reality that we hold on to that it shouldn't be like that, but it should be like we wanted to want it to be.
And this is already history since the change has already occurred.
We can put away any feeling, whatever it's about, and we will have brain ghosts and anxiety if we do not sort them out, live with them in some way or another.
