## Umdenken als Wendepunkt im Leben

<div class="handwritten">
Und welche Lösung siehst du denn? Schwebt dir das Umdenken vor? Die Orakelsprache des Körpers verstehen? Das Begreifen, dass der Körper kein Objekt abgesehen von mir selber ist, dass ich ein Körper bin und mir dessen bewusst?
</div>

Ja genau.
Spüre wie es sich in dir anfühlt, wenn du diese Fragen und Phänomene formulierst, wenn du sie nach unserem Gespräch beantwortest und genieße das Nachlassen der Unruhe und des Irrgefühls ... wenn es nun mal passiert.

Viele neue wissenschaftliche Arbeiten sind erschienen, die eine andere Denklinie verfolgen, die darin besteht, dem Körper zu helfen und die Symptome dorthin zu verfolgen, wo der "missing link" ist ...
zum Unvollständigen, zum Wackelkontakt im Immateriellen, zum Beispiel zu den Gedankenfehlern, Nanotechnik und Gen-Interpretation und -Verständnis, die Kommunikation beim Placeboeffekt unter anderen.

Aber viele Leute sind demgegenüber skeptisch, und vor allem gefährdet das den großen Umsatz der Arzneiindustrie, der heute das Bruttoinlandprodukt erhöht, und Klein-Schweden ist da keine Ausnahme.
Die Medikamente an sich gefährden die Umwelt, sind durch die ganzen Emissionen sehr schädlich, die durch unsere Toiletten und Wasserkläranlagen hindurchgehen, und das was sich danach abgesetzt hat, geht zur Erde als Düngemittel zurück und schadet sowohl Tieren wie auch Pflanzen.
Bei manchen verändern sich die Gene, bei anderen entsteht Unfruchtbarkeit ... aber trotzdem, Menschen verdienen Geld daran und das ist zum Göttlichen geworden in unserer Zivilisation.

Im Umdenken liegt daher auch ein tiefes Motiv des Überlebens, und vielleicht fällt dieses mit der Zeit dann leichter, wer weiß?

<div class="handwritten">
Ja, das Irrgefühl wegen der Umweltzerstörung macht mir auch Angst, ich meine, wenn wir, jeder von uns, die Umweltschäden begrenzen kann, vielleicht einfach nur, indem nicht Psychopharmaka und Antibiotika genommen werden, und wir kommen dennoch klar, so werden wir wohl eher motiviert.
Und trotzdem ... umdenken, wie machen wir das?
</div>

Gehe unser Gespräch zum Irrgefühl mehr als einmal noch durch und halte inne dort, wo ich davon spreche, wie man Bewertungen ’wäscht’, dann hast du wenigstens etwas zum Ausprobieren in der Hand.

Meine Sonderkenntnisse beziehen sich auf etwas, das ich Kommunikation als Kunst und primäre Arbeit und primäre Gedankenarbeit nenne.
Es ist eine Art sich selber freizugeben, aus dem eigenen Inneren heraus sich zu entwickeln, ohne etwas zum Anpassen zu haben.
Auf die gleiche Weise wie es der Mensch tat in der Kindheit der Menschheit, und wie wir selber das Leben entdeckt haben, bevor wir eine Menge Bewertungen angenommen haben, bevor unsere Eltern uns die Wirklichkeit so geordnet hatten, damit etwas da war, was uns gelenkt und erzogen hat.

Sich erlauben in der Welt der Phänomene sein zu dürfen, ändert im Körper nichts, sondern es hilft Menschen, Zufriedenheit, Geborgenheit und Gemeinsamkeit wieder einzuholen, von denen unsere Zellen aus den ersten 14 Tagen am Leben Erinnerungen haben, im Eileiter, nach dem wir entstanden sind und in denen wir ohne Angst leben, die neun Monate, die uns bleiben bis wir geboren werden und mit Unvollständigkeiten in Berührung kommen und erschrecken ... wo uns nichts bleibt als bewusst zu werden und uns zu entwickeln.

Die Erinnerungen, die wir aus dieser angstfreien Zeit haben, mit denen haben wir oft den Kontakt verloren ... und die Kunst ist ... an das, was innerhalb von uns selber da ist, das, was geblieben ist, wieder anzuknüpfen, was wir aber nicht gelebt und gepflegt haben seit wir Kinder waren und erwachsen wurden.

Ich hoffe darauf, dass einige von all den Leuten, die sich in der Kunst der Kommunikation bei mir geübt und ausgebildet haben, sich an dieses erinnern werden und vielleicht ... in der besten aller Welten ... bei dem, was sie im Leben tun, weiterentwickeln und sich verbreiten lassen und dass es außer mir noch Leute gibt, die dieses entdeckt haben, nicht zum Wenigsten in den verschiedenen Bereichen der Quantenwissenschaft.

Viele sind bei mir in Workshops zu Primärarbeit und zu primärer Gedankenarbeit gewesen und sie haben sich zurückbesonnen auf das, was da ist in ihnen selber ... das, was da ist, wenn nichts anderes da ist.
Ziemlich viele Leute meinen, sie haben davon unsagbaren Nutzen gehabt und ihr Irrgefühl ist dadurch weniger geworden, oder weitgehend ausgelöscht.

Dies heißt nicht, dass das Irrgefühl in neuen Gedankenfehlern nicht wieder erscheint, aber die Angst vor dem Irrgefühl nimmt ab und die eigene Fähigkeit es auszulöschen nimmt zu.
