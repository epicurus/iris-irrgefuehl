## Des Kaisers neue Kleider und die Politik

Ich meine, dass dies, die Aggressivität aufkommen lassen, anstatt in der Wut zu bleiben und eine konstruktive Lösung zu suchen, auch in der Politik geschieht.
Anstatt das Problematische bei dem, was der andere sagt, anzusprechen und eine konstruktive gemeinsame Lösung zu suchen, so konfrontiert man den anderen, damit er in Verteidigungsmodus kommt und ”an den Knöcheln abgesägt wird”, damit man selber Punkte gewinnt.
Es wird zum Gefühlsspiel, ein ”Herr im Hause”- Spiel, bei dem einer der Gewinner ist und die anderen Verlierer.

In der Kultur, in Filmen, in Serien, in den Schauspielhäusern usw, handelt es oft von diesem.
Die Wohlhabenden, die nicht an das Existenzielle denken müssen, sind dabei, Gefühlsspiele miteinander zu treiben, um einen Daseinswert zu spüren.
Die eigene Intimität mit sich ist bei ihnen schwach, denn sie wissen nicht, was sie in Wirklichkeit fürs Überleben tun können ... vom Geld abgesehen ... und dann machen sie anderen die Aufmerksamkeit streitig.

Sie werden verführt, sie bewundern etwas oder verleihen einer Person Macht, oder sie erfahren selber Bewunderung oder bekommen Macht und erfahren die Rückseite der Medaille und dann werden sie auf die eine oder andere Weise manipuliert und bekommen Angst, diese süßen Stückchen zu verlieren und werden dann verwundbar und man kann es ihnen leicht verderben, sie bekommen Irrgefühle.

Reiche Menschen sind damit so beschäftigt, dass sie gar nicht den Sinn am Leben im Leben und den Wert, der darin liegt, verstehen.
Stattdessen bekommen ihre Besitztümer und ihr Geld ihre Wertschätzung.
Das wird dem Primären vorgezogen, das belegt das Primäre und schafft viel Irrgefühl.
Es geht ihnen innen schlecht und sie leiden und es werden vielerlei Konzepte und Methoden erfunden, damit es ihnen besser geht, und für diese wird viel Geld ausgegeben.

Der Gegenpol davon sind die Armen, die wirklich nicht wissen, wie sie am nächsten Tag an Essen kommen sollen und die ganze Zeit Entbehrungen ausgesetzt sind.
Sie verstehen, dass das Leben das Wichtigste, und dass der Sinn am Leben das Leben ist, aber sie leben in der Angst, morgen ohne das Notwendige für Kinder und Familie zu sein.

Sie finden in einer politischen Gruppe zusammen, die für sie sprechen kann.
Während vieler Jahre war dies der Linksradikalismus und die Revolution, der Sozialismus und der Kommunismus waren der Gegenpol der privaten Politik eines Überflusses.

Sie waren nicht darauf aus, reich zu werden, nur wollten sie eine anders geartete Verteilung, sie wollten, dass die Reichtümer, die durch ihre Körperarbeit entstanden, vergütet werden und sie wollten ein allgemeines Sozialverteilungssystem, damit sie auch eine äußere Sicherheit hatten, auch wenn sie keine eigenen Wohnungen oder andere Güter besaßen.

Es kam zu einem extremen Gegenpol.
Heute ist der Gegenpol nicht links orientiert, sondern rechts orientiert und leider ist dies zum Nachteil der Bedürftigsten.
Sie verstehen selber nicht, dass das, wogegen sie ankämpfen, nicht ist, dass die Armen die Überlebenskosten bezahlt bekommen, sondern dass das ganze Steuersystem privatisiert wird, sodass die großen Gelder, die Gewinne in privaten Taschen landen, sodass die Reichen reicher werden und dadurch weniger Steuern zahlen, so dass weniger und weniger Geld übrig bleibt zum Verteilen an diejenigen, die dies wirklich brauchen.

Die Gehälter werden an den Markt angepasst, anstatt gemäß dem Notwendigkeitskonsum human angepasst zu werden, und dann können weniger Leute ein Gehalt bekommen und sich um die kümmern, die dies nötig haben.

Dieses Ökonomisieren schafft extrem viel Irrgefühl in uns als Menschen, da wir nicht verstehen können, wie es so viel Reichtum in einer Gesellschaft geben kann, und dass dies zu größerer Armut führt als es früher gegeben hat.
Wenn die Reichen weniger besteuert werden, und die Beiträge an die Arme gekürzt werden, so entsteht eine echte Unzufriedenheit, und leider ergießt sich dies darin, dass eine Gruppe von armen Leuten andere Leute als die Bedrohung sehen, nicht die geldprallen Taschen der Reichen.
Und dann kommt eine politische Partei, die Schwedendemokraten (AfD), als einzige alternative Partei auf.
Und ihre einfache Lösung ist einen Sündenbock und einen Gedankenfehler zu kreieren ... ”wenn wir nur keine Ausländer hätten, so hätten wir Geld für die Armen in Schweden”.
Das ist ein Majoritätsmissverständnis, das für einen Bumerang-Effekt sorgt, der viel panische Angst in der heutigen Gesellschaft hervorruft.
So, das braucht auf den Tisch und aufgedeckt werden.

<div class="handwritten">
Wie viele von denen, die ihre Stimmen den Schwedendemokraten geben, meinst du, sind da im Bilde und stimmen so, weil sie fest daran glauben, es ist wegen der Asylantenpolitik, dass die Steuereinnahmen nicht ausreichen?
</div>

Ich weiß es nicht, aber es sind wohl schon viele.
Ich denke, dass ich auf die einfache Lüge hereinfallen könnte, denn das Vergleichen fällt so leicht, und das Unwohlsein in mir verschwindet, wenn ich diese Bewertung annehme.

Das ist es, was man populistisch nennt, leicht zu gewinnende politische Punkte zu erzielen, und die große Masse dafür zu gewinnen, und im Falle der SD sehen wir ja davon die Folgen.

Was in der schwedischen Politik gerade geschieht, ist eine Variante des Märchens ’Des Kaisers neue Kleider’, du weißt, die Schwindler, die sagten, dass wer den Stoff nicht sah, sei entweder dumm oder untauglich für sein Amt.
Der König sah keinen Stoff (es gab ja nichts, was man sehen konnte), aber er wollte ja nicht dumm oder untauglich für sein Amt sein, und deshalb tat er so als ob es ihn gab.

Alle ließen sich durch diesen Mythos an der Nase herumführen, bis ein Kind sagte:
”Aber er hat ja nichts an!”
Die Leute versuchten das Kind zum Schweigen zu bringen, aber der Vater, auf dessen Schultern der Junge saß, von wo aus er den König sehen konnte, sagte ... ”Der Junge hat recht, ich kann auch keinen Stoff sehen, er hat nichts an”.
Ein Kind kann nicht dumm oder untauglich für sein Amt sein, das Kind sagt einfach wie es ist” ... und andere Leute schlossen sich dem an und alle fingen sie an über den König ohne Kleider zu lachen, und der eilte zum Schloss zurück und versteckte sich und schämte sich, dass man ihn so an der Nase herumgeführt hatte.

Auf diese Weise werden wir heute an der Nase herumgeführt, und ich frage mich wann jemand dies aufdeckt, so dass wir alle ausatmen können und Unmengen an Irrgefühlen loswerden ... und Burnout-Zustände und andere Zustände, die zu Irrgefühlen führen und von denen die Menschen heute betroffen sind.

Stell dir vor wie befreiend es wäre, wenn eine Person mit Führungsfähigkeit, mit hohem Code sagen könnte ... ”Aber an Geldern fehlt es nicht, es ist nur so, dass das Geld in privaten Taschen bei den Leuten landet,die schon reich sind und mehr und mehr und mehr haben wollen, anstatt in die Schatzkiste, aus dem die Beiträge für alle diejenigen geholt werden, die bei diesem ökonomischen System benachteiligt sind.”

<div class="handwritten">
Dann meinst du, dass alle Irrgefühle von diesen Gedankenfehlern und von den verdrängten Gefühlen, die in unserem Körper abgespeichert sind, herrühren, und die nicht eh..hm.. zu irgendeiner Art von Veränderung und Entwicklung geführt hat?
Und dass wir, was du Gedankenfehler nennst, nicht aufgedeckt haben?
</div>

Nein, nicht alles Irrfühlen kommt von Gedankenfehlern, das ist einer der Gründe.
Es passiert leicht, dass man sich wehrt, den Ernst dieses Gedankenfehlers zuzugeben, oder eher gesagt diesen aufzunehmen, und stattdessen macht man es zum Diskussionsgegenstand, nicht zu einem gemeinsamen Bewegen der Frage.
Das, worauf ich hinziele, ist gerade das Existenzielle bei diesem Gedankenfehler.
Wenn etwas unmenschlich statt menschlich wird und wir dies nicht bemerken, so geht das in ein Majoritätsmissverständnis über, das leicht dazu führt, dass wir uns über einen gemeinsamen Sündenbock einigen und ... ” wenn es nur keine ... dann wäre das Leben perfekt”.
Das ist ein Abhumanisieren, das verheerend wirkt.

Ich werde auch über andere Gründe sprechen und selbstverständlich gibt es Gründe von denen ich nicht weiß.
Aber dieser Gedankenfehler kommt oft in einer Zivilisation vor, die selten wirklicher existentieller Angst begegnet, bei der es ums Leben geht.
Dann entstehen Normen und Regeln und das schafft eine Kultur der Begrenzungen und des Richtigmachens und dann kann dies die Basis der Ängste sein, etwas, in das sich die Angst hineingießen kann.
