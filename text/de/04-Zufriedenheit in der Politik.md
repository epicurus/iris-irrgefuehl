## Zufriedenheit in der Politik

[//]: # "Frage: Warum hat noch keine politische Gruppierung dieses Zufriedensein im Hier und Jetzt aufgegriffen?"

<div class="handwritten">
Hmm ... das gibt mir wirklich zu denken.
Du scheinst dies so unglaublich sorgfältig durchdacht zu haben, während ich einfach weiterhin mein Dasein friste in dem, was da ist, aber du hast da eine Pointe, indem du auf den Unterschied zwischen Zufriedenheit und Unzufriedenheit hinweist, dass dies mit dem Vorwärtsgeist nicht so zusammenhängt.
Was ist der Grund, dass keine politische Gruppierung, während all den Jahren dieses aufgegriffen, und die politische Richtung geändert hat?
</div>

Das habe ich mir oft auch überlegt, aber ich habe keine Antwort darauf.
Vielleicht liegt dies daran, dass die Kultur in der Politik so ist, dass man einander ständig angreift und einander Bewertungen an den Kopf wirft und Punkte bei den Zuhörern dadurch gewinnt, dass man ein guter Rhetoriker ist, der spielend leicht die Argumentation vom Gegner zunichte machen kann.
Es gibt ja immer etwas an dem, was sie wollen und darstellen, was unvollständig ist, und da werden sie in eine Ecke gedrängt und wechseln zur Verteidigung, und gerade auf diese Weise geht das unter, was gut ist, bricht zusammen.

Ich glaube auch, dass Politiker normale Menschen sind, und sie werden auch Opfer eines idealen Gedankens.
Außerdem wollen sie nicht am Ast, auf dem sie selber sitzen, sägen.
Viele Politiker, die dieses Ziel verfolgen und ihr Streben so einrichten, bekommen einen Burnout.
Sie fühlen sich ausgenutzt und versuchen besser zu sein, als sie sind, und so kommt ein Reporter daher und deckt ihre Schlamperei oder sowas auf und so werden sie der Öffentlichkeit ausgehändigt und zum Mitläufer bei der Hetze, eine Form von Mobbing.

Ich glaube, dass viele nicht bemerken, dass dieses Sich-Anpassen an das Besser-zu-Sein-als-wir-sind, Stress bewirkt, der dazu führt, dass wir selbstdestruktiv werden, das Vertrauen in uns selber verlieren, und wir tun uns mit dem schlechten Selbstwertgefühl schwer, und dadurch nimmt das Vertrauen in uns selber ab, da wir naiv, unüberlegt handeln in Situationen, wo wir unter Druck sind.
Das schafft Irrgefühl.
So teilt der Körper unserem Bewusstsein mit, dass wir auf falschem Weg sind ... und wir hören da viel zu selten zu.

[//]: # "Frage: Wie kann man das gesellschafliche Zusammenleben besser gestalten? Wie kann man ein Land führen?"

<div class="handwritten">
Aber wie meinst du das eigentlich?
Deiner Meinung nach, wie soll man denn ein Land führen?
</div>

Ich meine, dass diejenigen, die zufrieden sind, zusammenwirken, zusammenarbeiten und zusammen sprechen wollen über die Phänomene, die problematisch sind, und sie durch Kompromisse lösen.
Nicht so, dass alle zur Hälfte zufrieden oder zur Hälfte unzufrieden werden, sondern eher, dass man etwas herausarbeitet, was zufriedenstellend, gut genug, was eine neue Form von Konsens ist.

Daraus folgt keine Sensation und das alles hat keinen besonderen Entertainmentwert in der Weise wie die Polarisierung eine Dynamik schafft, die spürbar ist: Es fehlen die witzigen Repliken, die das Publikum zum Lachen bringen und es durch Wortspiele und wie die Unterhaltung sich dreht, überraschen.

Die Gesundheit schweigt stille, sagt man im Schwedischen, sie macht kein Aufhebens, während das Ungesunde lauter Interesse und Engagement hervorruft, weil es abweicht.
Die Politik scheint auf das Abweichende zu bauen, nicht auf das Gesunde, aber ... warum man das Abweichende und nicht das Gesunde wählt, weiß ich wirklich nicht ...?
