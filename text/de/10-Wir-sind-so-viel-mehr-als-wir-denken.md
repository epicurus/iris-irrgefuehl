## Wir sind so viel mehr als wir denken

<div class="handwritten">
Das ist ja schon interessant zu wissen, um das zu verstehen, was du vom Irrgefühl sagst.
Wie das Irrgefühl eine Hilfe für uns ist, zu verstehen, wie wir in Gedankenfehlern steckenbleiben, und dass der Körper dann unsere Gefühle nicht entladen kann.
Aber ich glaube trotzdem nicht, dass die Leute das verstehen.
Ich verstehe es nicht, auch wenn ich höre, was du sagst.
Wie kannst du anderen helfen?
</div>

Einer der Gedankenfehler ist zu meinen, dass wir ein ICH, ein Ego sind, dass wir das sind, was wir sein möchten, indem wir die Eigenschaften züchten, die wir gernhaben.
Das Problem ist, dass wir selber als wir selber, so viel mehr sind und wir bewegen uns irgendwo zwischen dem Hellen in uns und dem, wo das Licht fehlt, dem Dunklen.
Wir haben vor dem Dunklen Angst, da wir nichts sehen, und auch wenn die gleichen materiellen Dinge im Dunklen da sind, die gleichen wie im Hellen, aber unsichtbar, jagt es uns Angst ein.

Das Problem mit dem Ego, dem ICH, ist, dass wenn wir nur dieses leben, so werden wir uns selber fremd am Rest, der auch im Dunkeln ist, gerade an den Eigenschaften, die ungebraucht da sind, und die meiste Angst bekommen wir gegenüber dem Immateriellen, mit dem wir nicht als mit uns selber gelebt haben, so dass diese Eigenschaften bekannt und gewohnt und geborgen sind.
Da das Benehmen, was daraus entsteht, vergessen ist, so sagen wir wenn dieses sich offenbart ... "ich war nicht ich selber als ich dies tat.
Es war als wäre es ein anderer gewesen und nicht ich.
Ich konnte mich nicht darin wiederfinden und ich bekam so einen Schrecken vor dieser Seite."
Zum Beispiel wenn wir eifersüchtig werden und direkt aus einem primitiven Autopiloten heraus reagieren, oder aggressiv und herablassend sind, wenn jemand aus Versehen etwas verwechselt und dadurch lügt, usw.

Das Immaterielle, was im Dunkeln ist, ist ohne Materie und vollkommen ungefährlich.
Wenn es sich dann als Eigenschaften herausstellt, die uns gehören, können die Reaktionen materieller Art werden und erschrecken, und das erzeugt Irrgefühl.

Das Dunkle enthält alle Information, die das Universum hat, und das Wenige, was uns unvollständigen Geschöpfen, die wir Menschen genannt werden, zugänglich ist, ist winzig, und das Immaterielle kann das Materielle nie verletzen, es ist nur das Materielle, was dem Materiellen schadet.
Zum Beispiel ... Wörter können weh tun und sie können verletzen oder erschrecken, aber sie schaden dem Materiellen in uns nicht ... dies heißt, dass wir im Grunde geborgen sein können, aber da wir finden, dass Menschen nicht in der Art etwas zu uns sagen dürfen, glauben wir, es ist gefährlich, und dann wird der Autopilot des Körpers angeführt und zum Anspringen gebracht, dieser erzeugt Gefühle, die wir nicht entladen, wir erkennen ganz einfach nicht, woher sie stammen, da Wörter ungefährlich sind, und dann bekommen wir stattdessen ein Irrgefühl.

[//]: # "Gedankenfehler: Worte und Gefühle anderer können verletzen"
[//]: # "Zitat: Der Hauptteil der Irrgefühle in dieser Zivilisation kommt von den Wörtern, von den Bewertungen, die  wir haben, von den Vorstellungen und den Idealen, die wir haben, wie wenn das gefährlich wäre, kommt von dem, dass wir angeführt werden zu glauben, dass wir zerbrechlich sind, wenn wir die Gefühle und Wörter anderer nicht ertragen, obwohl sie ungefährlich sind usw."

Der Hauptteil der Irrgefühle in dieser Zivilisation kommt von den Wörtern, von den Bewertungen, die wir haben, von den Vorstellungen und den Idealen, die wir haben, wie wenn das gefährlich wäre, kommt von dem, dass wir angeführt werden zu glauben, dass wir zerbrechlich sind, wenn wir die Gefühle und Wörter anderer nicht ertragen, obwohl sie ungefährlich sind usw.
Dieses magische Denken ist angsteinjagend und voller Irrgefühl, und obwohl es elementar ist, nicht existentiell, so halten wir daran fest, dass es in anderer Weise schädlich ist, als es ist.

In früheren Zeiten waren es die Obersten in der Gesellschaftsordnung, die von Irrgefühlen betroffen waren, denn sie wurden auf eine Weise erzogen, wo die Wörter verletzt haben, und Reaktionen bei anderen verursachte Unbehagen, was als Angst empfunden wurde, und hierdurch wurde das immaterielle Irrgefühl geboren.
Man sagte dazu "schlechte Nerven" haben oder man sei überempfindlich oder spröde.

Heute ist dies bei den meisten Menschen üblich und es wird behauptet, man solle nicht jedes Wort einfach über sich "ergehen" lassen, sondern wirklich mit Abweisung, Trennen, Bedingungen und Strafe reagieren.
Das Problem daran ist, dass wir selber Schiss bekommen, wenn wir dies gegenüber anderen tun, weil es eine so furchtbare Konsequenz ist, einsam zu werden, und abseits der Schar zu geraten.

Wenn wir andere nicht als menschlich ansehen, sondern als böse ansehen, und dass es da seine Berechtigung hat, Menschen von der Gemeinsamkeit auszuschließen, so tragen wir selber einen Schaden davon.
Das schafft eine Menge Irrgefühl, was den Willen und die Tatkraft im alltäglichen Leben lähmt.
Die Menschen werden unfähig.
Dieser Bumerang-Effekt, ist schwer zu verstehen, und es fängt damit an, dass wir einfach wissen, dass das, was wir selber tun, auf uns zurückschlägt und unsere eigene Geborgenheit gegenüber anderen mindert.
Davon zu wissen ist weitreichend.
