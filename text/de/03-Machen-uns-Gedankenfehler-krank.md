## Machen uns Gedankenfehler krank?

[//]: # "Frage: Machen Gedankenfehler, dass wir uns anpassen und krank werden?"

<div class="handwritten">
Ich werde niedergeschlagen, wenn ich an all die Gedankenfehler denke, die ständig in meinen Bewertungen auftauchen, die ganze Zeit.
Ich tue ja immer alles bewerten, und ich vergleiche, damit ich leicht Entscheidungen treffen kann und einen eigenen Willen habe, damit die Leute finden, ich kriege es gut hin.
Ich passe mich allem und jedem und allen meinen Vorstellungen, wie es sein soll, an, und ich bin sehr gut darin und ich habe oft Irrgefühl und habe gemeint, es soll so sein und dass es so ist, weil ich nicht gut genug bin.

Ich setze mich noch mehr unter Druck und fühle mich ganz fertig, und ich höre andere, die das Gleiche sagen, es sei alles viel zu viel und das Tempo zu hoch, und die Zeit reicht nicht usw.
Alles wird anstrengend und ich habe Angst ein Burnout zu kriegen ... und das kriege ich und bin außer Gefecht und bin krankgeschrieben wegen hohem Blutdruck, Migräne und schlechtem Magen und Überempfindlichkeit.

Meinst du, dass all dies daran liegt, dass wir Gedankenfehler haben, die machen, dass wir uns anpassen und davon krank werden?

</div>

Ja, aber ich werde einen kleinen Umweg machen, um diese Frage zu beantworten.

Neun Monate in der Gebärmutter, sind wir ganz zufrieden, wir sind in äußerer Geborgenheit und die ersten 14 Tage sind wir ganz autonom, eins mit uns selber.
Wir brauchen nichts von außen, aber wir entwickeln uns unglaublich schnell und effektiv.
Wir bringen alles, was wir zum Überleben brauchen, in unserer Konstitution mit, denn es gibt uns ja offensichtlich immer noch, alles entwickelt sich einfach, und das erfahren wir die neun Monate, die wir innerhalb eines anderen Menschen in extremer Geborgenheit verbringen.
Wir erinnern uns daran, es gibt das in unserem autonomen System, und es wird uns Impulse geben, Dinge zu tun, seien wir noch so zufrieden ... vielleicht werden diese Impulse sogar mehr und besser, wenn wir zufrieden sind als wenn wir unzufrieden sind.

Wir bringen Neugierde zur Erforschung der Wirklichkeit mit und das ist der tiefste Entwicklungsimpuls, den es gibt.
Außerdem sind wir kreativ, operativ und wollen unsere Welt und Wirklichkeit erforschen, unsere Wissbegierde bringt uns dazu, dass wir, wenn wir zufrieden und innerlich geborgen sind, uns wirklich vorwärts bewegen und die Kultur erweitern, verbreitern und vertiefen, und das ist der wirkliche Reichtum, im Unterschied zu dem angepassten Vorwärtsgeist, der aus der Unzufriedenheit hervorgeht, weil wir das aufgestellte Ziel nicht erreicht haben.
Dann leben wir stattdessen von einem Später ... wir schieben die Befriedigung auf und leben von der Hoffnung, sie später zu kriegen, gleich: dann, wenn wir das Ziel erreicht haben, dann werden wir glücklich leben, unseren Lebtag lang.

[//]: # "Gedankenfehler: Wenn wir nur genug vorarbeiten, dann können wir irgendwann sorglos und glücklich leben, für den Rest unseres Lebens. Dann haben wir ausgesorgt."

Dieses Ideal ist ein Gedankenfehler.
Er ist so groß, dass er zu einem Majoritätsmissverständnis geworden ist.
Niemand glaubt noch an das Sein, und dass es im Sein immer vorangeht.
Wir können nicht rückwärts gehen, so sehr wir uns auch dagegen sträuben, es geht immer voran.
Ich habe nie jemanden gesehen, der als Alter anfängt und jung wird, sondern alle kommen ins Leben, werden geboren und leben eine Reihe von Tagen, enden, und so kommen neue Generationen, die leben und die ihre Kultur prägen.

Nun ... es gibt einen Film, wo ein Mann alt geboren wird und am Ende ein Kind wird, aber ... es gibt in der Wirklichkeit keine Anzeichen, dass es so ist.
