## Üben wollen, oder perfekt sein wollen

[//]: # "Gedankenfehler: Es ist schlecht, Fehler zu machen, und es ist gut, perfekt zu sein"

Es so anzuschauen, dass ich es falsch mache und falsch bin, statt perfekt, ist ein anderer Gedankenfehler, der zu Irrgefühl wegen der Leistung führt und zu Panik davor, etwas auszuprobieren, anstatt es so anzuschauen, dass ich übe bis ich es kann.
Dann sind Fehler harmlos, sie sind nur ein Schritt auf dem Weg hin zum Können.

Ich möchte dir nur ein anfängliches Verständnis geben, dass ein Irrgefühl nicht etwas Mystisches, Magisches ist, was von außen kommt und dich überfällt, sondern es ist etwas, was dein Körper erzeugt, wenn du nicht in deinem Tempo, in deiner eigenen Unvollständigkeit lebst, und dich so lange darin übst bis du gut genug bist ... wenn du nicht in deinem eigenen Gemütsstrom lebst, den du hast, wenn du zufrieden bist ... wenn du nicht hier und jetzt anwesend bist, sondern in Vorstellungen bist, die entweder Vergangenheit oder Prognosen der Zukunft sind, etwas, was du in Bewertungen festmachst, anstatt hier zu sein und das zu üben, von dem du weißt, dass du es können wirst, wenn du es dir von Grund auf beibringst.
Wenn du, mit deinem Bewusstsein, kontrollieren möchtest, wie andere dich wahrnehmen sollen.
Wenn du gewisse Eigenheiten, und Schwächen verbirgst, weil du nicht perfekt bist, und wenn du andere Eigenschaften in den Vordergrund rückst und sie zum Perfektsein erhebst ... dann züchtest du Ängste, die zu Irrfühlen werden.

Es gibt eine Menge Ideale und Gedankenfehler, die wir erfüllen wollen, die wir vor uns als Ziele hingestellt haben, um gute Menschen zu sein, statt uns selber, als normale unvollständige Menschen zu sein, und das führt zu Anpassung, die in Trotz umschlagen wird ... was an uns zehrt, so dass wir schach-matt-müde und uns selber, anderen und dem Leben feindselig werden.
Wenn wir dann auf Abstand gehen, weil wir bemerken, dass wir sonst vom Uns-Anpassen gemein werden, so entdecken wir, dass wir einsam sind und uns unglücklich fühlen ... Was wir da machen isoliert uns und führt nicht dazu, dass wir die Zufriedenheit innen behalten, so dass wir Genugtuung spüren, und wir bekommen kein Glücksgefühl, sondern wir lösen das, in dem wir uns abtrennen.
Wir trennen uns von anderen, und dann geht es uns davon schlecht.

<div class="handwritten">
Ich werde neugierig auf das, was du über Irrgefühle sagst und auch auf die Gedankenfehler, von denen du sprichst.
Kannst du das noch weiterentwickeln, bevor du anfängst, von anderen Arten von Irrgefühlen zu reden?
</div>

Klar kann ich das.
Ein kleines Kind übt bis es kann.
Wenn ein Kind einen Erwachsenen laufen sieht, auch wenn es sehr schnell krabbeln kann, so wird es neugierig auf das Laufen.
Dann üben Kinder das Gleichgewicht, stellen sich hin, plumpsen auf ihren Hintern, stellen sich hin ... gehen seitlich nebenher und sie üben, üben, üben.
Das Misslingen zählt bei ihnen nicht, sie schreien das nur so heraus und machen weiter, denn sie wissen, dass sie können, aber noch klappt es nicht so gut.
Nach langer Zeit und vielem Üben können sie strauchelnd dahergehen und am Ende können sie gehen und rennen und sich tanzend leicht bewegen.
Das ist das Einzige, was bei ihnen zählt, dass es möglich war, dies zu erüben.
Sie wurden vertraut damit, dass das Lernen so geht.

Als Erwachsene ist uns die Idee gekommen, dass wir etwas können sollen, ohne durch Übung zu dem zu kommen, und da werden wir enttäuscht, wenn es uns sofort nicht gelingt. Und dann führt diese Enttäuschung dazu, dass wir das so hindrehen, dass wir schlecht sind, weil wir nicht können und wir entwickeln ein Irrgefühl der Leistung.
Dies kommt vom Gedankenfehler, dass nur weil wir intellektuell wissen wie es ist, wenn es gelungen ist, so meinen wir, wir könnten es ohne Üben, und dann spielen unsere Gefühle und Gedanken uns einen Streich und bringen uns dazu, mit unserem Bewusstsein alles kontrollieren zu wollen, und das geht nicht, weil es in der Konstitution ist, wo wir uns üben und uns etwas einverleiben brauchen, um uns zu entwickeln, und das geschieht, wenn wir üben und das führt immer dazu, dass wir können.

[//]: # "Frage: Warum schämen wir uns, etwas nicht zu können, etwas erst üben zu müssen?"

<div class="handwritten">
Weshalb meinst du, schämen wir uns vorm Üben?
</div>

Ich meine es liegt daran, dass der Körper dann so froh und zufrieden wird und wir sollen ja unzufrieden sein, um voranzukommen.
Dieser Gedankenfehler plagt diese ganze Zivilisation dorthin, dass es normal wird, wenn es einem schlecht geht ... "so geht es ja jedem", es wird allgemein vorherrschend, aber es ist nicht natürlich.
Das Natürliche ist, dass es einem gut geht, wenn im Moment das eigene Leben nicht bedroht ist.

[//]: # "Zitat: Das Natürliche ist, dass es einem gut geht, wenn im Moment das eigene Leben nicht bedroht ist."

Es reicht, wenn bei uns nur das zählt, was uns gelingt.
Mit dem Anderen können wir als Übung rechnen und dann bekommen wir kein Irrgefühl wegen der Leistung oder auch keine panische Angst, wenn wir nicht sofort können.
