## Wie Gedankenfehler im Leben wirken

[//]: # "Frage: Warum macht es so viel Angst sich im Innern zu verändern? Warum habe ich so viel Angst davor? Ich will das und dennoch habe ich demgegenüber Unmengen an Widerstand und Trotz."

<div class="handwritten">
Warum macht es so viel Angst sich im Innern zu verändern?
Warum habe ich so viel Angst davor?
Ich will das und dennoch habe ich demgegenüber Unmengen an Widerstand und Trotz.
Ich werde böse mit dir und denke, du hinderst mich und machst mir das Leben unbequem.
Ich will ganz einfach nicht.
Ich möchte das alles schnurstracks gerade gehen soll, und will das Umständliche und das Unbequeme, von dem du da redest gar nicht üben.
</div>

In dieser Kultur, in dieser Zivilisation, wo wir einen Überglauben an die Fähigkeit des Intellekts, alles bewältigen zu können, geschaffen haben, und wo wir meinen, durch ein rationales Denken Glück und Befriedigung von außen bekommen zu können, sind wir total an der Nase herumgeführt worden, und das sind wir und deshalb entsteht so viel Irrgefühl, dass Menschen Medikamente nehmen, um das Leben in diesen Gedankenfehlern noch zu managen.

[//]: # "Gedankenfehler: Wir vertrauen auf das, was wir technisch geschaffen haben und verlieren aus dem Auge, was in Erfahrung und Vertrautheit schon seit vielen tausend Jahren da ist."

Ein anderer Gedankenfehler ist, dass das, was wir technisch geschaffen haben, uns dazu veranlasst auf etwas 100-150 Jahre Altem zu vertrauen, und nicht auf das, was als Erfahrung, Vertrautheit seit vielen Tausend Jahren da ist, was von der Überlebenserfahrung und Überlebensprägung herrührt ... dass daran noch etwas wäre.
Da vertun wir uns ordentlich.

<div class="handwritten">
Nenne mir ein einzelnes Beispiel davon, wie wir das Moderne zum Richtigen werden lassen, und wie wir dadurch ein Irrgefühl bekommen, so kann ich vielleicht verstehen, wie Irrgefühl entsteht.
</div>

Ein Beispiel aus meinem eigenen Leben war die Geburt meiner Tochter in den 60er Jahren, und als sie gestillt werden sollte, so platzten schier meine Brüste vor Milch.
Da sagte mir die Krankenschwester, dass ich so viel Milch wie möglich rauspumpen sollte, damit meinem Kind nicht zu viel Milch übrigblieb, denn dies war nicht gut.
Ich tat dies pflichtbewusst und was geschah?
Im Moment, wo sie trank, ging es sehr gut, sie war nach etwa 15 Minuten satt und danach schlief sie zufrieden ein.
Wie ging es bei mir?
Nun, mein Körper produzierte noch mehr Milch, denn er meinte, dass alles, was ich rausgepumpt hatte von Kindern aufgebraucht worden war.
Daher bekam ich nur mehr und mehr Milch, egal wie viel Milch ich da rauspumpte.
Mein Körper meinte, dass ich statt einem Kind Drillinge ernährte.
Die Milch strömte unaufhörlich nach und tropfte ständig, ich war die ganze Zeit nass, egal wie viele Einlagen ich auch hinein tat ... daher gab man mir ein Pumpe mit nach Hause, um die Milch aufheben und einfrieren zu können, um später dem Krankenhaus zu bringen, denn es gab immer welche, die keine Milch in ihren Brüsten bekamen und diese Milch war dann immer gefragt.
Je weniger diejenige, die wenig Milch hatte, pumpte, desto weniger Milch produzierten ihre Brüste und hörten damit auf, da ihre Kinder, an der Flaschenmilch, die man ihnen gab, satt wurden.
Verstehst du den Gedankenfehler, wenn die Milch rausgepumpt wird, und wenn nicht gestillt wird bis dahin, dass die Milch vom eigenen Körper produziert wird?

<div class="handwritten">
Ja, ich bin dabei.
Dann verstehe ich wesentlich besser, was Gedankenfehler mit uns machen.
</div>

Das Kind entwickelte nicht seine eigene Kraft, Anreize für die Produktion zu geben und bei extrem vielen ging die Milch aus, weil wir das von außen regelten, anstatt dass Kind und Mutter geübt haben bis es ging, wie es Tausende von Jahren geschehen ist, als die Technik nicht da war.

Alle, die Kinder bekamen, sorgten sich, dass die eigenen Kinder nicht ausreichend Milch von der eigenen Brust bekommen würden und so fingen sie mit Ergänzung an, um sich so sicher zu fühlen.
Sie bekamen Irrgefühl und wurden gestresst und dann, erst recht, hörte ihre Produktion von Milch auf.

Kannst du durch dieses Beispiel verstehen, dass die moderne Aufklärung, die Technik solle das Problem von außen lösen, ein Gedankenfehler ist?
Aber es gibt kein Übel, mit dem nicht auch was Gutes einhergeht.
Ich pumpte aus meinen Brüsten so viel Milch und bekam vom Krankenhaus Geld dafür, und für dieses Geld konnten wir uns ein nagelneues Auto kaufen, und darüber freuten wir uns sehr.
Kommst du jetzt dem Verstehen von Gedankenfehlern näher, die so üblich werden, dass sie zu Majoritätsmissverständnissen werden?

<div class="handwritten">
In mir ist etwas, von dem ich berührt werde, und es fühlt sich so an, als würde ich anfangen zu ahnen, was du meinst.
Mich befallen oft Unsicherheit, Unruhe, Stress und es fällt mir da schwer zu wissen, was ich bei verschiedenen Situationen tun soll.
Ich bemerke, dass das, was du unter Irrgefühl verstehst, ein Chaos, ein Verwirrtsein in mir ist und das ist oft schmerzlich.
Da kommen mir Impulse, mich zu wehren oder mich aus dem Staub zu machen, oder dann entsteht, was ich Wut nenne, was über andere herzieht, über die Kinder oder über meinen Mann.
Ich verstehe jetzt, du meinst, dies seien Aggressionen, die aus Angst kommen, und die ich nicht fühle, sondern ich bemerke nur die Wut, die aufbraust, und die mich dazu bringt, mich bei jemandem oder bei etwas rächen zu wollen.
Iii ... da fühl’ ich mich schlecht und bekomme noch mehr Irrgefühl.
</div>

Ja, das ist es, was ich den Bumerang-Effekt nenne, der das Irrgefühl nicht gelindert hat, sondern zurückkam und dich selber traf und dir noch weiteres Irrgefühl gab.
In diesem Hamsterrad rennen wir oft einfach weiter, weiter, weiter und das liegt an einem Gedankenfehler ...
Ist das möglich zu verstehen?

<div class="handwritten">
Hmm ... ich werde mir das noch überlegen.
Ich schreibe es mir auf.
Ich hoffe das klärt sich mit der Zeit.
</div>
