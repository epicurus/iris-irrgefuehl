## Offene Fragen

<div class="terminal">

WILLKOMMEN im Garten des Epikur!

Hier kannst du eigene Fragen zum Heft hinzufügen.
Das sieht zunächst vielleicht etwas ungewohnt aus, aber das ist ein Weg (fast) direkt in das Heft hineinzuschreiben ...
Alles was du machen musst, ist auf dieser Seite weiter oben auf 'Edit' zu klicken, dich einzuloggen (beim ersten Mal musst du vielleicht noch ein Konto bei Bitbucket anlegen)
und dann weiter unten unter die Beispielfrage etwas hinzuzufügen.
Bei den drei Punkten.
Und dann zweimal 'Commit' klicken.

Zweimal deshalb, weil du (noch) nicht ein Projektmitglied bist, sondern ein Besucher.
Deswegen wird deine Änderung nicht direkt ins Heft geschrieben, sondern erst zum Projekt geschickt.
Dort schaut das jemand an und klickt dann auf ok.
Und dann ist die Frage im Heft und wird beim nächsten Mal drucken mitgedruckt.
Und auch zu Iris geschickt, vielleicht kommt sie dann ein wenig später weiter vorne als eigenes Kapitel ins Heft.

Und lass dich nicht von den Befehlstexten dazwischen irritieren ...

</div>

<!-- if booktype is epub -->[Bitbucket Repository](https://bitbucket.org/epicurus/iris-irrgefuehl/src/develop/text/de/24-Offene-Fragen.md) <!-- endif booktype -->
<!-- if booktype is pdf -->![Offene Fragen](media/qr-code2.png) <!-- endif booktype -->

- Beispielfrage: Wie häufig kann man Irrgefühl im Leben erleben? Jeden Tag? Oder seltener?

- ...

<div class="title-page">&nbsp</div>
<div style="visibility: hidden;">\pagebreak</div>
