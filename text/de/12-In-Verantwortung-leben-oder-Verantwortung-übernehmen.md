## In Verantwortung leben oder Verantwortung übernehmen

<div class="handwritten">
Wie meinst du das, sollen wir nicht Verantwortung übernehmen, soll alles einfach nach Belieben gehen und chaotisch werden, und vielleicht beim Schlimmsten enden?
</div>

[//]: # "Frage: Iris? Wir sollen nicht Verantwortung übernehmen? Soll dann alles einfach nach Belieben gehen und chaotisch werden? Und dann vielleicht schlimm enden?"
[//]: # "Gedankenfehler: Es ist gut, und oft sogar notwendig, Verantwortung für etwas zu übernehmen"

Noch einmal ... Verantwortung ist ein Zustand, kein Produkt.
Du kannst eine Aufgabe übernehmen, das ist aber nicht dasselbe, wie wenn du Verantwortung übernimmst.
Wenn man glaubt, es ist möglich, Verantwortung zu übernehmen, ist ein Fehldenken dabei, es ist wirklich ein gigantisches Majoritätsmissverständnis.
Dieser Gedankenfehler erstickt Freiheit in dieser gesamten modernen Zivilisation, der Massen, dass wir Roboter erschaffen, die alle verantwortliche Produktarbeit tun sollen, damit wir frei sein können.
Schaue, was bei uns gerade im Gange ist.

<div class="handwritten">
Ja, wäre es denn nicht gut, dass Roboter alles Notwendige tun, so dass wir die Unfreiheit endlich los sind?
</div>

Ich möchte nur sagen, Verantwortung nicht zu einem Produkt machen, denn das ist sie nicht.
Verantwortlichsein ist ein Bewusstsein und eine Fürsorge für das Unvollständige, und es ist etwas, in dem wir leben, wenn wir gerade an etwas Unvollständigem etwas tun können.
Das Vollendete braucht das Verantwortlichsein nicht, nur beim Unvollständigen, brauchen wir etwas unternehmen, und so eine Sache ist gerade, wenn unser Ideal nicht der Wirklichkeit standhält, zum Beispiel
wenn jemand zu einem Termin oder zur Schule oder, wenn das Essen fertig ist, zum Essen zu spät kommt, usw.
usw.

Weißt du, was wir verlieren, wenn wir aufhören, selber die Dinge zu tun, die notwendigkeitshalber geschehen? Wir verlieren das Gespür für das Leben, wir vergessen die Vertrautheit mit der Erde, der Welt und der Wirklichkeit.
Wir verpassen, uns wertvoll zu wissen, indem wir da sind und tun können, all das, wozu wir selber in der Lage sind, bis ins Alter hinein.
Dieses befriedigende Tun bleibt aus, und stattdessen bekommen wir eine Menge Irrgefühl.
Dieses produziert sich, indem unser Vertrauen in die eigene Fähigkeit verlorengeht und dann verschwindet der Weg zum Selbstgefühl, der da ist, an den wir aber nicht herankommen.
Es bedeutet Unbefriedigung innen und Sinnlosigkeit, was zur Gleichgültigkeit führt, was wirklich Depression ist.
Dem folgt ein Irrgefühl, wegen unserer Unfähigkeit, da mit unseren destruktiven Impulsen, zum Beispiel mit unseren Selbstmordimpulsen umzugehen.
Das Irrgefühl tritt da auf, um uns daran zu hindern, diesen Impulsen zu folgen, und wenn wir dann Medikamente zu uns nehmen und das Irrgefühl hinwegdämpfen, so ist das Risiko groß, dass es uns gelingt, uns das Leben zu verkürzen.

Das Irrgefühl als Verbündeter, als Abwehrmechanismus verträgt sich mit dem Gesellschaftsideal nicht ... wir können uns nicht passiv diesen Idealen unterordnen und dabei leben ... schnell, effektiv, rationell und bequem uns selber und anderen ... dann vermasseln wir es, es geschehen bei uns Verwicklungen und wir verteidigen uns und wehren uns gegen die Erziehung und gegen die Anforderungen dieses Gesellschaftsideals.
Wir werden nichts-tuende untaugliche Parasiten = mit Burnout, ermüdet, allergisch, fibromyalgisch = an den Steuergeldern zehrend.
Eine sehr krasse Herausforderung für die Politik.

Im Primären gibt es keine solchen Ideale, da ist überhaupt nichts vorgegeben.
Im Primären sind wir bloß.
Wir sind ein Sein.
Es gibt uns, wir leben und haben alle Zeit der Welt ununterbrochen unser ganzes Leben, und da denken wir nicht innerhalb des Gesellschaftsideals, denn gerade da, gibt es nichts besseres, es ist gut genug = von Innen zufrieden, zum Überleben das zu tun, was notwendig ist, und wenn dann uns noch Zeit bleibt ... mit allem Reichtum spielen, der im Leben da ist.
