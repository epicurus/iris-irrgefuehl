## Das Irrgefühl und die Unvollständigkeit

<div class="handwritten">
Warum fällt einem dieses Denken, dass du vorstellst, so schwer? Mir ist es so unbequem und unlustig, dass ich am liebsten nur sagen möchte, okay, na und, und dann weitereilen zu dem, was Spaß macht.

Irgendwie will ich nun mal nicht versuchen zu verstehen, dass Irrgefühl nicht etwas ist, was mich von außen befällt und bei dem ich etwas kriegen kann, was es wegschafft ... eine therapeutische Methode, Medizin, Yoga oder eine andere alternative Behandlungsmethode.

Es ist so unbequem zu denken, dass es in mir erzeugt wird, davon, wie ich in der Wirklichkeit denke und handle.
Das heißt, dass ich schuld bin, dass ich Irrgefühl habe, obwohl ich keine Ahnung habe, was ich tue, damit ich es kriege.
Das fühlt sich nach etwas an ... ich weiß nicht nach was, aber nach etwas an, wogegen ich gar nicht ankomme.

Ich verstehe ja, dass der Unterschied ist, dass du aus all dem heraus denkst, was es an sich gibt, und womit wir Probleme bekommen, wenn wir uns hindern, darauf eingehend zu leben, und dass ich denke, dass ich meinen Spaß haben will, dass es feierlich, spannend und glücklich zugehen soll.
Es ist, wie wenn dein Denken das nicht ist, aber ich verstehe, dass dieses trotzdem irgendwie so ist.
Es ist nur nicht verlockend nur zu SEIN ... sondern ich möchte Gefühlserlebnisse bekommen, die etwas mit mir machen ... ich glaube, das ist es, was du Kicks nennst.

Warum werde ich von all dem anderen angezogen, statt von dem ersten? Warum ist die Versuchung so groß, drängen die Gefühle so sehr, im Sekundären zu leben und von den Warnsignalen, die das Gewissen sendet abzusehen?
Ich verstehe, dass der Körper das Sein will, aber der Kopf und die Lust wollen die Gefühlserlebnisse, die Kicks.
Hiiiiilfe!!!

</div>

Bei dem was du sagst, ist was dran ... Ich denke, nicht, dass du schuld an etwas bist, oder dass du ein besserer Mensch sein sollst als du bist.
Ich denke, dass die Unvollständigkeit, die macht, dass wir überhaupt leben, dieses Unbehagen in uns verursacht, und dass dieses Unbehagen nur dazu da ist, dass wir nicht aus Unwissenheit total schieflaufen, ohne dass der Körper reagiert ... es ist nur so.
Ich weiß, dass es auch im Körper das gibt, was ich ein gesundes Gewissen nenne, ein System, das sagt Warnung Warnung Warnung, ein Spüren, dass etwas nicht mit rechten Dingen zugeht, dass wir dadurch, von den Erfahrungen, die wir haben, anfangen können Gebrauch zu machen, und das Unbequeme darin auf uns zu nehmen, zu versuchen gewisse Dinge zu ändern, damit das Unbehagen weggeht, Verhaltensweisen, die uns fremd sind, zu üben, und auszuprobieren, ob sie gut sind, wenn sie uns bekannt und gewohnt geworden sind.

Diese Möglichkeit gibt es statt des schlechten Gewissens und des Irrgefühls.
Aber ich meine, dass die meisten Menschen hin und her wechseln zwischen diesen beiden Verfassungen und dass die ganze Konsumgesellschaft von dieser Versuchung Gebrauch macht, die du veranschaulichst, um selber davon zu profitieren ... oder um des ökonomischen Zuwachses Willen, wenn wir diesen rechtfertigen wollen.

Wenn Menschen verstehen, dass Irrgefühl ungefährlich ist, wollen sie vielleicht lieber damit zufrieden sein, dass sie mit ihrem Irrgefühl unzufrieden sind ... als weiterhin das Besitztum zu vergrößern und zu glauben, das Glück kommt später.
Vielleicht würden sie gerne das Ohne-Irrfühlen-Sein ausprobieren, hier und jetzt ... wer weiß?
Was das Problem ist, so wie ich es sehe, dass ... indem wir das Wort Irrgefühl geschaffen haben, indem wir eine Diagnose dieses unangenehmen Zustandes bekommen haben, so hören wir auf, praktisch Verschiedenes zu tun, um das Irrgefühl sich auslöschen zu lassen, und stattdessen sehen wir es als eine Sache, eine Diagnose, als eine Einheit an, die wir erklären können und für die wir umsetzbare Methoden finden können, um es zu eliminieren ... und dies statt des Natürlichen, mit ihm zu leben und zu verstehen, dass es etwas ist, worüber wir von Innen, gemäß dessen nachdenken brauchen, damit der Körper selber es erlöschen lässt, wirkliche Selbstheilung.

Ich möchte nur die Alternative zum Sekundären geben, dass ein Lebensweg da ist, der schafft, dass wir Liebe in uns selber aufziehen, dass wir das Leben mit anderen teilen, dass wir als Konsequenz Glück spüren und zufrieden sein können, ohne Bewertungen als Kompensation zu produzieren.

So verhält es sich bei sehr vielen Dingen.
Wenn wir das Primäre, was nur an sich selber da ist, zu etwas Sekundärem machen, zu etwas Elementarem und Wählbarem, so fangen wir sofort an, nach etwas von außen zu suchen, was das Üble hinwegschaffen kann, ohne dass wir uns aufraffen und das Unbequeme auf uns nehmen brauchen, uns zu verändern, bei dem innerlich heranreifen, damit der Körper selber Beistand hat, uns am Leben zu halten.

Mir wird das so deutlich bei der Diabetes.
Als man in Schweden diese Krankheit Zuckerkrankheit nannte, sah jeder ein, dass es Leute gab, die vom Zucker krank wurden, und dass sie ihre Diät ändern mussten, damit sie ein Minimum an Zucker enthielt.
Dann hatte der Körper auch andere Hilfe nötig.
Man verstand, dass der Körper nicht das Hormon produzierte, das den Zucker abbaute und da kam das Insulin, damit man dieses von außen zuführen konnte, um es von innen wirken zu lassen und der Zuckerkranke überlebte noch länger, trotz dieser tödlichen Krankheit.
Dieses änderte nichts an der Tatsache, dass diejenigen, die die Zuckerkrankheit hatten, keine Süßigkeiten essen sollten, denn das belastete das körperliche System zu sehr.

Wie ist es heute?
Nun, zum Beispiel viele, die die Diagnose Diabetes haben, essen Süßigkeiten wie die es tun, die diese Diagnose nicht haben.
Sie erhöhen nur die Dosis an Insulin, da sie an einem Messstab sehen können, ob der Blutzuckerspiegel sinkt oder nicht.
Außerdem können sie ein Depot im Körper einoperieren lassen, das die ganze Zeit die rechte Menge an Insulin zum Abbau absondert ... aber das Problem, dass der Zucker anderes Gewebe und Teile im Körper abbaut und schädigt, bleibt, die Nieren, die Leber, das Blut usw., daher hat es dennoch ein Sinn, den Zucker so gering wie möglich zu halten, aber die meisten denken, dies ist in einer entfernten Zukunft, und fragen nicht danach.

Es ist zum Ding geworden, zu etwas, das man managed, statt etwas zu sein, mit dem man lebt und die Gewohnheiten der eigenen Lebensführung demnach ändert, auch wenn man zusätzlich mit Insulin nachhelfen muss.

Dabei geht das Schöne an den Leuten vorbei, lieb mit sich selber zu sein und dadurch Mengen von Freude im Körper, Helligkeit und Zufriedenheit an der eigenen Innenseite zu bekommen.
Leute wissen das nicht, und deshalb meinen sie, es sei etwas Negatives, die Lebensführung gemäß einer Krankheit zu ändern.
Ich möchte dies nur sagen, ich will keine Moralisierung oder Begrenzung, nur dass ein Gewinn darin liegt, alte Gewohnheiten, bei denen das Haltbarkeitsdatum abgelaufen ist, zu ändern, die Milch wird schlecht und fault am Ende ... wie alle wissen.

</div>
Ja, aber ist es nicht gut, dass sie normal leben können, und am Diabetes doch nicht krank werden, dass sie Medizin haben, so dass sie nicht mehr zuckerkränklich sind, wie du sagst?
</div>

Klar ist es gut, dass es unterstützende Medikamente gibt, die bewirken, dass wir Diabetiker ein normales Leben führen können, aber ...

Nein, es ist auch nicht gut, wenn wir nicht dem eigenen Körper liebevoll und fürsorglich sind, sondern einfach meinen, er ist ein Objekt, ein Ding, dass er nur eine Hülle für ’mich selbst’ ist und soll funktionieren, ganz ohne, dass ich meine Verantwortung lebe, wenn wir davon absehen, dass ich selber sein heißt, mein Körper und mein Bewusstsein zu sein, dann entsteht Ego-abgefahrenes Lust-Kick-Denken statt Ausgewogenheit und das schafft Unkontakt und Irrgefühl.

Es ist ja in der Ausgewogenheit, wo wir Liebe zum Gefühl aufziehen, es ist in der Ausgewogenheit, wo wir Glück empfinden, es ist in der Ausgewogenheit, wo wir mit innerer Geborgenheit in Berührung kommen, die wir während der Schwangerschaft empfinden, die es möglich macht, unserem Dasein in der eigenen Fähigkeit von innen her zufrieden zu sein, zu vertrauen, genauso wie wir es die Monate sind, wo wir in einem anderen Menschen leben.
Das Bleiben, gerade hier und jetzt, Gegenwart, ist hier jetzt anwesend, uns selber, wir selber, ich selber vollends, im Grund im Körper zufrieden.

Alles, was wir objektifizieren, indem wir aus ihm sekundäre Dinge werden lassen, die man bewerten kann, wo es nicht um das Leben, so wie es ist, geht, wird uns verrücken uns selber und verdinglichen, und dann kann dies ökonomisiert werden, als gut oder schlecht, richtig-falsch und ob es sich lohnt oder nicht.

Dann erwarten wir, Gesundheit und Heilung von außen zu bekommen, in der Form, dass wir es kaufen und besitzen können, und von dem wir erwarten, es hält uns am Leben, schenkt uns Liebe und Glück, Zufriedenheit und Freude.
Es ist selbstverständlich, dass wir das erwarten können, und wenn das nicht mit der Wirklichkeit übereinstimmt, dann ist es natürlich, unzufrieden damit zu sein, dass das Leben so gemein ist, dass wir Irrgefühl kriegen und da soll dieses erstmal entfernt werden.

Da wird das Leben nicht mehr bequem, nicht mehr schnell und effektiv, es wird nicht mehr rationell, sondern stattdessen werden wir ausgeschieden, haben Burnout, werden voller Irrfühl und zu unfähigen Parasiten der Zivilisation, wir werden an den Steuermitteln zehren anstatt Nahrung der Zuwachsökonomie zu sein, und wenn das so gekommen ist, können wir wie das Irrgefühl weg ... damit niemand Geld für uns ausgeben muss ... die, die zehren kommen weg, damit der Rest von uns, die Elite, die gesund ist, nicht genötigt wird, für diejenigen, die nicht mehr können, bei diesem Systemfehler, diesem Majoritätsmissverständnis Geld auszugeben.

[//]: # "Gedankenfehler: Gesundheit und Heilung kommt von außen, wir heilen den Körper oder unser Gemüt, dadurch, dass wir außen etwas tun"
