## Willkommen im Garten des Epikur\*

Dieses Heft ist aus Gesprächen entstanden und es können weitere Gespräche daraus folgen ... und das Heft darf dadurch anwachsen.
Tragt gerne eure weiteren Fragen auf der nächsten Seite „Offene Fragen“ ein.
Dort findet ihr einen QR code.
Fragen zum Thema Irrgefühl können in Workshops mit Iris dann weiter bearbeitet werden.

Die Karten mit Fragen auf den folgenden Seiten sind als Hilfestellung gedacht, falls einige von euch Lesern sich gerne physisch oder virtuell trefft, um über die Phänomene im Heft gemeinsam zu sprechen.
Wenn da jemandem keine eigene Frage an die anderen einfällt, kann eine Karte gezogen werden (sie können ja herausgeschnitten werden und gestapelt bereitliegen).
Es ist ja auch möglich, auf die Karten zu verzichten und zu hoffen, einem fällt keine Frage ein, und dann zu schauen, ob der Leistungsdruck spürbar wird, viellicht sogar das Irrgefühl vor der Leistung aufkommt.

<div class="title-page">&nbsp</div>
<div style="visibility: hidden;">\pagebreak</div>

<div class="handwritten">
Wurdest Du mit Gewalt bedroht? Erzähle – wenn du magst – wie es war.
</div>

<div class="handwritten">
Hast du Irrgefühl gespürt?  Erzähle – wenn du magst.
</div>

<div class="handwritten">
Magst du beschreiben wie deine kräftigste Erfahrung des Irrgefühls war, bei dir selber oder bei einem anderen?
</div>

<div class="handwritten">
Welche Gefühle (Freude, Trauer, Angst, Wut) durften in deiner Urprungsfamilie da sein?
</div>

<div class="handwritten">
Welche Gefühle  (Freude, Trauer, Angst, Wut) durften nicht in deiner Urprungsfamilie da sein?
</div>

<div class="handwritten">
Erzähl - wenn du magst - von einer Situation, in der du in Lebensgefahr warst.
</div>

<div class="title-page">&nbsp</div>
<div style="visibility: hidden;">\pagebreak</div>

<div class="handwritten">
Bist du zufrieden am Leben?
	- Beschreibe wie du zufrieden bist.
	- Beschreibe wie du unzufrieden bist.
</div>

<div class="handwritten">
Wie versuchte dein Vater besser zu sein als er es war?
</div>

<div class="handwritten">
Wie versuchte deine Mutter besser zu sein als sie es war?
</div>

<div class="handwritten">
Vor welchen Wörtern hast du Angst?
</div>

<div class="handwritten">
Vor welchen Bewertungen hast du Angst?
</div>

<div class="handwritten">
Wie hat sich deine Angst vor dem Tod ausgedrückt?
</div>

<div class="title-page">&nbsp</div>
<div style="visibility: hidden;">\pagebreak</div>

<div class="handwritten">
Gibt es in deinem Leben Situationen oder Zeiten, wo du damit versöhnt warst zu sterben?
Magst du etwas davon erzählen?
</div>

<div class="handwritten">
Darfst du am Leben Freude haben?
</div>

<div class="handwritten">
Beschreibe - wenn du magst - wie du in einer Situation versucht hast, besser zu sein als du bist.
</div>

<div class="handwritten">
In deinem Leben jetzt: Was macht dich froh?
</div>

<div class="handwritten">
In deinem Leben jetzt: Was macht dir Angst?
</div>

<div class="handwritten">
In deinem Leben jetzt: Was macht dich wütend?
</div>

<div class="title-page">&nbsp</div>
<div style="visibility: hidden;">\pagebreak</div>

<div class="handwritten">
In deinem Leben jetzt: Was macht dich traurig?
</div>

<div class="handwritten">
Erzähle - wenn du magst - wie es war, als du in deinem Leben am wütendsten warst (nicht aggressiv).
</div>

<div class="handwritten">
Erzähle - wenn du magst - wie es war, als du in deinem Leben am meisten Freude hattest.
</div>

<div class="handwritten">
Erzähle - wenn du magst - wie es war, als du in deinem Leben am traurigsten warst.
</div>

<div class="handwritten">
Erzähle - wenn du magst - wie es war, als du in deinem Leben die stärkste Angst gespührt hast.
</div>

<div class="title-page">&nbsp</div>
<div style="visibility: hidden;">\pagebreak</div>

<div class="handwritten">
Erzähle - wenn du magst - wie es war, als du in deinem Leben am aggressivsten gehandelt hast.
Welche Eigenschaften sind dein Herzstück?
</div>

<div class="handwritten">
Hast du eine Ursprungsphobie?
Magst du beschreiben, was bei dir passiert, wenn sie losgeht?
</div>

<div class="handwritten">
Welchen Eindruck möchtest du nicht bei anderen hinterlassen?
</div>

<div class="handwritten">
Welchen Eindruck möchtest du bei anderen hinterlassen?
</div>

<div class="title-page">&nbsp</div>
<div style="visibility: hidden;">\pagebreak</div>

<div class="handwritten">
Kennst du die Bumerangwirkung bei dir selber?
Magst du erzählen?
</div>

<div class="handwritten">
Kennst du die Bumerangwirkung bei anderen?
Magst du erzählen?
</div>

<div class="handwritten">
Beschreibe - wenn du magst - was dir die Technik bedeutet.
</div>

<div class="handwritten">
Erzähle - wenn du magst - eine Art von Situation, der du aus dem Weg gehst, wo aber dein Leben nicht bedroht ist.
</div>

\* Über Jahre hinweg hat Iris im Sommer täglich eine Fragestunde unter einem Baum gehalten.
Sie saß in der Mitte und um sie herum, Kinder, junge Erwachsene, Ältere, die gefragt haben, was sie wollten und Iris hat geantwortet.

<div class="title-page">&nbsp</div>
<div style="visibility: hidden;">\pagebreak</div>
