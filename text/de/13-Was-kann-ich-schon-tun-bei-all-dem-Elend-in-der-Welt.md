## Was kann ich schon tun bei all dem Elend in der Welt?

<div class="handwritten">
Oh, es klingt so einfach wenn du es sagst, aber bei mir klappt das gar nicht.
Ständig sorge ich mich wegen all dem Negativen, was mir wirklich die Ruhe verdirbt, alles was ich vom Elend der Welt weiß, und von dem, wie wir das Klima zerstören und all, all, all das ... alles im Fernsehen, alle Nachrichten, alles im Netz, alles Schreckliche, von dem die Menschen reden, was so schnell geht und was die ganze Zeit näher rückt.
Was soll ich, Kleines ... tun????
</div>

[//]: # "Frage: Ich sehe all das Negative in der Welt, das mir die Ruhe nimmt, das ganze Elend der Welt, im Fernsehen, in den Nachrichten, im Internet. Und es rückt die ganze Zeit näher. Was soll ich als kleiner Mensch da tun?"

Wenn wir umdenken versuchen, und es ohne jegliche Bewertung tun und bemerken dabei, dass das Irrgefühl weggeht, dann verstehen wir, dass es stimmt, dass es unsere Bewertungen und unsere Sorge wegen der Zukunft sind, die das Irrgefühl hervorrufen, und dann haben wir das Werkzeug, die Vertrautheit damit, auch wenn das Umschätzen nicht leicht fällt, unbequem und schmerzlich ist, bekommen wir dann keine Angst und keine weiteren Irrgefühle.
Das gibt Hoffnung wegen der Möglichkeit, und das reicht, damit wir die Zufriedenheit innen spüren und da kommen wir auf die Geborgenheitsseite und verstehen.
Da wird im Körper kein Irrgefühl erzeugt und dann ist keine Gereiztheit nötig, um den Schmerz zu lindern, denn es entsteht kein negativer neuer Schmerz.

Damit habe ich das Leben verbracht.
Zuerst selber die Gedankenfehler verstehen, dann bemerken welche allgemein sind und viele Leute steuern, die Majoritätsmissverständnisse, du erinnerst dich ... und dann in Worte fassen, wie wir natürlich tun und was das außer Spiel setzt ... was wir tun, wie wir tun, wie wir denken, und was wir stattdessen denken und tun können.

Es ist selten, dass wir ein Fehldenken entfernen können, aber wir können ein anderes Denken oben drauf tun und je bekannter und gewohnter das wird, desto weniger Irrgefühl bekommen wir.
Es kommt bei uns ein Dulden der Unvollständigkeit und wir werden stärker und standhafter, wie wir es üblicherweise nennen, bekommen eine größere Ausdauer u.a.

<div class="handwritten">
Hmmm ... aber die Psychiatrie und die Psychologie setzen ja Medikamente und Therapien ein, damit wir nicht bei dem Irrgefühl landen.
Ich höre nur ganz wenige, die wie du von dem Anderen reden.
Warum ist das so?
Ist es so, weil andere, genauso wenig wie ich, wissen, was ein Irrgefühl ist?
Nicht verstehen, dass es gestoppte Gefühle sind, die nicht zur Reaktion kommen und nützlich werden ... Erfahrungen werden ... für Entwicklung sorgen, wie du sagst?
</div>

Ja, wie gesagt, ein Großteil der Irrgefühle entsteht aufgrund dessen ... so scheint es wohl zu sein.
Ich kann dies nicht beweisen und ich bin kein Forscher und habe auch nicht sonderlich viel Forschung auf diesem Gebiet gefunden.
Aber es ist so praktisch einsetzbar, dass man es im Leben ausprobieren und es selber herausfinden kann.
Dies ist, was man geprüfte Erfahrung nennt und das gehört auch zur Naturwissenschaft.

<div class="handwritten">
Das Entladen wie du sagst, fällt mir schwer.
Die Gefühle, ich weiß, dass ich sie habe, aber sie kommen nur dann auf, wenn ich einen Film schaue, der mich berührt, ein Buch lese, durch das sich meine Gefühle einschalten, aber das führt bei mir nicht dazu, dass meine Bewertungen sich im Grunde ändern?
</div>

Nein, das stimmt.
Es ist gut, dass du überhaupt Gefühle spürst und dich in solche Situationen begibst, die du mir nennst.
Ich nenne dies gefühlsmäßige Nebenarbeit tun, und das kultiviert Mitgefühl und Verständnis und bereichert unsere Erfahrung, und das ist gut.
Besonders wenn es zu Überlegungen zum Leben und zum Sinn am Leben beim Leben, und zu dem, was im Leben wichtig ist, zu Beziehungen, führt.
Aber dies kann auch einfach Ablenkung sein, und dann ist der Nutzen einfach der, dass wir uns eine Weile entspannen können, von dem, was uns bedrückt, oder wir fühlen uns von dem Irrgefühl für eine kleine Weile befreit.
Das auch ist gut genug.
