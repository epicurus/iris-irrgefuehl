## Ich will eine gute Mutter sein

<div class="handwritten">
Ich möchte hören, wie es sein würde, wenn wir tun würden, was du als das Natürliche ansiehst.
Irgendein Beispiel, das allgemein verbreitet ist, was wir zum Normalen rechnen.
</div>

Klar ... Wie folgt ist es oft, wenn wir Kinder kriegen.
Wir denken dann wir wollen eine gute Mutter sein und tun unser Bestes, um dies zu sein, ohne dass wir geübt haben.
Und dann wird es sofort eine Anpassung an die Idee, wie eine gute Mutter ist, anstatt, dass ich ich selber bin als Mutter und lerne, indem ich übe.
Wenn wir uns keine Gedanken darüber machen, fehlt uns das Vertrauen darin, dass wir gut genug und mit dem zufrieden sind.
Wir erlauben uns nicht, etwas nicht zu können und dann zu üben, bis wir gut genug sind, und das, was wir können brauchen, können.

<div class="handwritten">
Gilt das auch für dich, hast du selber versucht, eine gute Mutter zu sein und dich anzupassen?
</div>

Als ich mit meinem Kind vom Krankenhaus nach Hause kam, hatte mir die Krankenschwester gesagt, dass ich nicht öfter als jede vierte Stunde stillen durfte.
Meine Tochter hatte nach 3 Stunden Hunger und wimmerte und quengelte und weinte eine Stunde lang.
Ich fragte meine Mutter, was ich in dieser Lage tun sollte und was nun das Gute an diesem Abwarten sei?

Meine Mutter sagte, es sei nur eine neue Erfindung, und sie fragte mich, wie lange ich meinte, hatten die Leute nach der Uhr gestillt und nicht nach dem Hunger des Kindes?
Ich sah ein, dass sie Recht hatte und kümmerte mich nicht mehr um die Uhr oder die Wissenschaft und hörte auf die Ratschläge der älteren Frauen.
Ich prüfte diese Ratschläge aus und übte stattdessen und es ging mir gut und ich hatte vor dem Fehlermachen keine Angst.

[//]: # "Gedankenfehler: Wir wissen, was man als Mutter oder Vater für ein Kind tun muss"

Je früher wir Kinder kriegen, je jünger wir sind, desto besser, denn dann fühlen wir nicht so stark die Erwartungen und die Anforderungen.
Es ist natürlich, dass wir dann nicht so viel können, deshalb dürfen wir üben und wir bekommen oft die Unterstützung und Hilfe durch die Älteren.
Aber wenn eine Mutter in ihren 30er-Jahren ist und hat schwanger werden können seit sie etwa 15 ist, so wird von ihr die unmittelbare Entwicklung zur guten Mutter erwartet.
Das ist ein Gedankenfehler, dies entwickelt sich erst, wenn wir Kinder haben und üben.
Das ist so gängig, dass "jeder das weiß", was gar nicht wahr ist, nur weil so viele das meinen.
So wird es zum Majoritätsmissverständnis und führt zur Anpassung und zu Irrgefühl der Leistung und stellenweise zur Panik.
Es passiert bei Müttern leicht, dass sie deshalb Gemeinheit in Bezug auf ihre Kinder entwickeln, etwas, das quasi Geburtspsychose oder Stillpsychose genannt wird, ein Zustand, in dem man gefühlsmäßig betäubt ist und nicht die Beiderseitigkeit mit seinem Kind erlebt.

<div class="handwritten">
Kann das so ernst werden?
</div>

Ja, aber gewöhnlich ist es nicht so, sondern gewöhnlich führt die Unsicherheit dazu, dass wir Bestätigung haben wollen, dass wir gut sind, und dies wird dem Kind später oft ein Problem.
Am Anfang gibt das Kind sich damit zufrieden, dass die Mutter zufrieden ist, aber mit der Zeit wird die Entwicklung des Kindes von dem ständigen Achtgeben darauf, wie es der Mutter geht, gehemmt.
Wenn die Mutter in diesem Zustand des Tüchtigseins ist, sucht sie nach Bestätigung, dass sie eine gute Mutter ist, nicht nur nach der Bestätigung ihrer selbst als Mutter, und wenn sie etwas leisten braucht, dann braucht das Kind zu leisten.

Oft möchte sie dann gesagt bekommen, dass sie eine gute Mutter ist, und dann wird das Kind dies tun, damit sie zufrieden ist.
Es ist den Kindern äußerst bedrohlich, wenn es der Mutter nicht gut geht, sie in Irrgefühlen landet und nicht in der Begegnung anwesend ist.
Da erlebt das Kind, dass sein Leben bedroht ist.
Das Kind geht mit der Mutter in die Symbiose, mischt sich in die Gefühle der Mutter ein und gibt das Zufriedensein in sich selber auf.
Nur wenn die Mutter das ist, ist es das auch.

Die Mutter fühlt dann ein angenehmes Gefühl der Bestätigung und tut alles für ihr Kind, bedient es und ist ihm Hubschraubermutter ...
Wenn das Kind älter wird, so hemmt dies das Kind, und es fängt an unzufrieden zu werden.
Das beunruhigt die tüchtige Mutter und sie fühlt sich, wie wenn sie nicht ausreicht, nicht gut genug ist und sie bekommt ein schlechtes Selbstgefühl, und dies mindert ihr Vertrauen zu ihr selbst.
So, es hilft nicht, wenn man versucht tüchtig zu sein.
Es ist eine Herabwertung in Bezug auf uns selber, wie wenn wir nicht gut genug sein würden, indem wir wir selber sind, wie wir sind, mit Fehlern und Mängeln und Verdiensten.
Letzten Endes werden wir bezweifeln, dass wir gut sind und werden angepasst und misstrauen uns selber, und ermüden.

Es ist ein Irrgefühl wegen der Leistung, das uns ermüdet, wenn wir meinen wir sind nicht so, wie wir sind, gut genug und so zufrieden, auch wenn wir manchmal schlechter sind als am Besten.
Aber wir können üben und uns ändern und üben und üben, bis das Neue dasjenige ist, was gilt und selbstverständlich ist.
Leider hindert es uns, dass wir die Information, wir sind gut genug und ohne Leistung damit zufrieden sein können, nicht bekommen.

Verstehst du wie ein Gedankenfehler sich so ausbreiten kann, dass niemand daran denkt, dass er ein Fehler ist, weil Leute meinen, es ist nur das, was als normal zählt, das gilt?

<div class="handwritten">
Ja, ich sehe dies wirklich ein, und ich bekomme Angst, wenn ich dir zuhöre, aber das Irrfühlen wird mir klarer, es bleibt nicht beim Zustand, den wir ertragen müssen, sondern wir können uns Gedanken dazu machen, und dadurch das Bedürfnis des Körpers mindern, Irrgefühle zu erzeugen.
Du sagtest was darüber, dass es auch andere Arten von Irrgefühlen gibt?
</div>

Es ist Anpassung an ein Ideal und sie führt dazu, dass du nicht du selber bist, sondern dass du die Eigenschaften bist, von denen du dir vorstellen kannst, dass andere diese gern haben ... sie werden zu deiner Identität und deshalb wirst du verspannt und unnatürlich und müde, ausgelaugt, und dir bleibt nichts anderes übrig als dich zurückzuziehen und ’die Batterien aufzuladen’, um Kraft zum ’Dichproduzieren’ zu haben ... es ist an Stelle von entspannend natürlich zu sein als der, der du selber bist.
Das führt zu Ängsten, die in deinem mentalen Erleben verzehrend sind und zu Ermüdungs- und physischen Leiden werden, die schmerzen.
