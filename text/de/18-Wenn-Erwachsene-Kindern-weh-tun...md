## Wenn Erwachsene Kindern weh tun ...

Dann ... jetzt werde ich von einem anderen Irrgefühl sprechen.
Von einem Irrfühlen, das vom Körper wegen etwas erzeugt wird, was uns lebensbedrohlich sein kann, vor allem wenn wir Kinder sind.
Nur der Körper ist sterblich und daher ist es nur das Irrgefühl wegen der Körperangst, an das wir nicht rankommen.
Wir können nur mit der eigenen Urteilsfähigkeit uns selber helfen, dem nicht Opfer zu werden, sondern stattdessen Geschicklichkeit im Umgang mit ihm zu entwickeln und mit ihm klarzukommen.

Wenn Erwachsene Kindern drohen, ihnen weh zu tun, und wenn Erwachsene Kinder schlagen, werden sie außer sich vor Furcht, und der Körper reagiert damit, den Autopiloten einzuschalten.
Entweder verschwindet der Kontakt mit dem Fühlen und mit dem Denken innen und die Person landet in einem Vakuum.
Es kommt zu einer paralysierten Lage.
Entweder hört dann der Angriff auf oder das Kind wird weiterhin geschlagen.
Oder auch versucht die Person sich zu verteidigen und ihre Unschuld oder den Grund des Geschehenen zu erklären und da entstehen meist Lügen.
Der Erwachsene wird dann noch aggressiver und wir bekommen deshalb noch mehr Prügel.
Wenn wir so viel Angst haben, können wir nicht im Körper bleiben, auch wenn wir das möchten, gerade weil der Eindruck da ist, unser Leben ist bedroht und da erfinden wir Wahrheiten, die uns selber wahr sind, die aber nicht mit der Wahrheit übereinstimmen.

Oder auch springt die Hochoperativität an und wir fliehen Hals über Kopf und danach vermeiden wir den ’gefährlichen’ Erwachsenen so weit möglich und haben in etwa so viel ’Vertrauen’ gegenüber der Freundlichkeit dieser Person, wie gegenüber einer Klapperschlange.

Oder wir machen Gebrauch von unserer Stärke und von unserer Gescheitheit, gehen unfair vor und verletzen den Erwachsenen, um ihn unschädlich zu machen, damit er niemandem weiteren wehtun kann.
Misshandlung und Totschlag sind eine übliche Folge davon, wenn das Kind ein wenig älter ist und durch einen Erwachsenen angegriffen wird.

Wenn einer der Erwachsenen in der Nähe des Kindes seinen Partner misshandelt, so wird das auch vom Kind als existenziell erlebt, wie wenn es direkt es selbst betroffen würde und dadurch bekommt das Kind den gleichen Schock, den gleichen Schrecken, das gleiche traumatische Erleben.

Diese Erlebnisse bleiben in der Erinnerung des Kindes stecken, so wie sie in dem Moment erlebt wurden und sie werden diesen Menschen für den Rest seines Lebens hemmen und begrenzen, es sei denn dieser bekommt Hilfe.

Es brennt sich dermaßen ein, selbst als Erwachsener, wird die Person innen klein und wehrlos sein, wie sie es bei der Gelegenheit war, als der Schrecken zum ersten Mal kam, und dies wiederholt sich immer und immer wieder.
Mit diesem Irrgefühl durch das Leben zu gehen ist nicht leicht, es bringt oft Selbstdestruktivität, Hass und Rachegefühle hervor und als Folge davon Schuld- und Schamgefühle, sowie schlechtes Gewissen und Selbstverletzungen.
Die Folge sind so hohe Anforderungen an einen selber, dass man sie unmöglich erfüllen kann, und da hat man einen Grund sich selbst zu bestrafen.
Dabei dreht es sich im Kreis = was einem angetan worden ist, tut man sich oft selber an.
Alles, um das scheußliche Irrgefühl bei einem selber zu besänftigen, und weil man selber glaubt, man ist wirklich böse und schuldig, weil jemand einen so behandelt hat.

<div class="handwritten">
Was du mir jetzt erzählst, macht mir Angst.
Ich meine ja, die meisten Kinder bekommen einen Klatsch auf den Hintern, oder man packt sie am Ohr oder jemand packt unsanft ihnen am Arm ... meinst du das verletzt sie genauso schlimm?
</div>

Nein, das meine ich nicht.
Die meisten Kinder wissen, dass ihre Eltern Fehler und Mängel haben und wissen, dass sie ihnen als Kinder manchmal blöde Sachen antun, und sie verzeihen das sehr schnell.
Es kommt bei ihnen nicht zur physischen Angst, sondern sie wimmern und schreien ’aua, aua’ usw, und sie kommen leicht davon, weil das Verhalten des Erwachsenen einfach daher kam, dass der Sinn für einen Moment übergeschwappt ist und das macht dem Körper des Kindes nicht sonderlich viel Angst.
Sie sagen schnell ’entschuldige, entschuldige, ich habe es nicht so gemeint’.
Meistens ist das nicht wahr, aber das reicht dem Elternteil und er hat dadurch einen legitimen Grund zum Umdenken.

Aber ... es ist wichtig, dieses physische Irrgefühl von allem psychischen Irrgefühl zu unterscheiden, denn vom Immateriellen bekommt der Körper keinen Schrecken, selbst wenn der Autopilot angeworfen wird und wir reagieren wie wenn es lebensbedrohlich wäre und so fühlt es sich auch an.
Aber wenn es nicht lebensbedrohlich ist, ist der Körper im Grunde geborgen und da ist es leichter das Erlebte zu verarbeiten.

Das Materielle ... die Hand eines Menschen, die das Materielle schlägt ... das Gesicht eines Menschen ... das ist schädlich und gefährlich, und das jagt dem Körper adäquaterweise Angst ein, anders, als ich das beschrieb.

Das wird sich irgendwann später zeigen, wenn das Kind älter wird.
Zum einen in den Teenagernjahren, wo dieses Kind in der Schule oft Schwierigkeiten hat und oft aus der Gruppe ausgestoßen wird.
Zum anderen, dass der Teenager nach Außen gewalttätig reagiert, halbkriminell wird, sich gerne prügelt, Drogen testet und früh im Drogenkonsum steckenbleibt.
Zum Dritten, dass der Teenager nach innen reagiert, indem er unsicher wird, meint, er könne es nicht hinkriegen, er sich selber nicht erlaubt, dass es ihm gelingt, er möchte nicht großwerden oder erlaubt es sich nicht von andern geschätzt zu werden u.a.

Der Autopilot diktiert Bedingungen dafür, wie die Person die Wirklichkeit wahrnimmt.
Er springt ständig an und lenkt hin zum schlimmsten Misstrauen gegenüber einem selbst und anderen ... und wenn jemand versucht nett zu sein so wird dem misstraut, als hätte derjenige verborgene Absichten und ich selber kann nicht offen und verletzlich sein, sondern bin berechnend, wie ich etwas sage, was ich sage und was ich tue.
Alles ist genau kontrolliert.
Daher wird es ermüdend mit anderen Menschen zu sein und man kann nicht glauben, dass jemand einen gernhat, so wie man ist, sondern man glaubt, dass Leute einen ausnutzen wollen.

Die Person wird innen vor der Intimität mit sich selber abgeschnitten, und die Beiderseitigkeit mit anderen fällt ihr schwer.
Dies kann herabgelindert werden, wenn die Person sich verliebt und die Liebe erwidert wird und dann kann es eine Weile gut gehen aber ... oft ist es schnell vorbei.

<div class="handwritten">
Das scheint heute das Übliche zu sein ... ist es einfach das oder fällt es nur mehr auf?
Außerdem bekomme ich bei dem Gedanken Angst, dass es bei vielen innen so schlimm ist, und ich habe keine Ahnung, wie ihnen geholfen werden helfen kann.
Also ... hilf mir ...
</div>

Das existentielle Irrgefühl, das sich bei Kindern einschaltet, wenn die Erwachsenen bedrohlich sind, vor allem wenn es um die Eltern geht, das Bedrohliche, was die Kinder zum blinden Gehorsam gegenüber den Erwachsenen bringen will, steuert zur Paralysierung vor allem bei Mädchen ... und dies führt dazu, dass wenn sie erwachsene Frauen geworden sind, geraten sie in diese Verfassung, wenn ein Mann sexuelle Gefühle hat ... es wird wie bei ihrer Kindheit, als der Vater aggressiv und bedrohlich war ... Paralysierung ...

Wenn diese Mädchen den Schrecken zum blinden Gehorsam nicht bekommen hätten, dann hätten Frauen, auf eine ganz anderen Weise sich wehren können, wenn jemand versuchen würde sie zu vergewaltigen, aber jetzt ist es oft so, dass sie stattdessen paralysiert werden und aus dem Schrecken heraus aus ihrem Körper hinausgetrieben werden, und immateriell daneben stehen und zuschauen, wenn jemand sie vergewaltigt ... das schafft eine Menge Irrgefühl im Körper, das für einen selbst schwer fassbar ist, und noch schwerer ist, bis zu den Gefühlen zu folgen und sie zu entladen, da man sich so schuldig fühlt.
Dann wird es schwierig aus dem Geschehenen zu lernen, um dies zur Erfahrung für das nächste Mal werden zu lassen und zur Geschichte.

Wenn kleine Mädchen Mütter gehabt hätten, die sich eingeschaltet hätten, und sie vor dieser Aggressivität geschützt hätten, und ihnen gesagt hätten, sie können zu Mama kommen, denen sie hätten sagen können, sie haben Angst, und die Mütter hätten sich um sie gekümmert.
Dann hätte dies aufgelöst werden können, aber die Mütter haben oft den gleichen Schrecken und werden genauso paralysiert und sind für die Mädchen daher keine Hilfe.
Und dadurch wiederholt sich die Geschichte.

Wenn Mädchen in ihren Teenagernjahren eine Initiation zum Frausein mitmachen würden, bei der sie Training ihrer eigenen Stärke erhielten, so kraftvoll zu sein, wie sie sind, und wenn sie in Situationen geraten, wo sie gefährdet sind, auch Zugang zu ihrer Hochoperativität haben, so würde diese Paralysierung vielleicht ganz anderen Verhaltensweisen weichen, die Vergewaltigungen hindern würden.
Dass sie bei dieser ihre Hochoperativität üben dürfen und statt dem anderen treten, schreien, beißen, damit sie Zugang zu ihrer Wut und zu ihrer wirklichen Kraft bekommen ... und dann kann es vielleicht in Zukunft anders werden und vielleicht können Männer Frauen als Menschen respektieren, anstatt sie nur als Objekte zu sehen.
Das Problem bleibt bestehen, aber vielleicht wird es kleiner und das ist gut genug.

<div class="handwritten">
Ja, das wäre gut ... ich hätte selber gewünscht, mich hätte die Aggressivität der Männer nicht so beeinflusst und erstarren lassen, und ich spüre ihre frustrierte sexuelle Lust auch.
Das ruft Irrgefühl bei mir hervor und ich verschwinde aus dieser Situation so schnell wie möglich.
</div>

Als Erwachsene hast du ein Urteilsvermögen ... du kannst durch deine Reife die Gefahr einschätzen, aber als Kind fehlt dir diese Fähigkeit, du bist nicht entwickelt, und das klammert sich fest in deiner Erinnerung und entwickelt sich nicht, daher fühlst du dich so klein.
Aber es ist gut, dass du jetzt verstehst den Gefahren auszuweichen.
