## Was können wir mit dem eigenen Irrfühlen tun?

Viele Menschen suchen einen Arzt auf, weil es ihnen schlecht geht, weil sie Irrfühlen haben und sie verstehen nicht woher dieses kommt.
Viele bekommen dann Medikamente verschrieben und werden abgeschnitten von dem Irrgefühl und dann fühlt es sich besser an ... aber leider, das Abgeschnittensein schafft auch Irrgefühl.
So, nach einer Zeit haben die Medikamente keine Wirkung, sondern wirken dem entgegen und dann hat man sowohl das ursprüngliche Irrgefühl und noch dazu das Irrgefühl wegen dem Abgeschnittensein.

<div class="handwritten">
Aber Iris ... das klingt ja ganz aussichtslos, gibt es denn überhaupt einen Ausweg?
Was sollte man tun, anstatt diese Irrgefühle zu schaffen, und wenn sie entstanden sind, was macht man dann, wenn man keine Medikamente nimmt?
So wie du es sagst, liegt es daran, dass wir Bewertungen haben, die nicht möglich sind, da diese Bewertungen schlimme Irrgefühle hervorbringen?
Die von uns, die betroffen sind, verstehen dies ja nicht wenn wir aufwachsen, und wir werden ja beeinflusst von den Meinungen der Umgebung und davon, wie diese denkt, und wir meinen, dass das richtig ist ... oder ... wir reflektieren nicht einmal darüber ... ganz einfach.
</div>

Ziemlich viele dieser Bewertungen bekommen wir durch die Erziehung, dadurch, dass unsere Umgebung findet, wir sollen tüchtig, brav, ordentlich sein, u.a.
Es ist im Grunde eine Herabwertung, da wir das alles an sich selber sind und mehr noch.
Außerdem sind wir unvollständig, unentwickelt auf manchen Gebieten, und überentwickelt auf anderen, und daraus erwachsen uns im praktischen Leben Probleme.

Es ist nicht möglich, das zu erschaffen, was schon da ist, ohne dass daraus bewertende Bewertungen entstehen, die schaden ... nun ist es so in der sekundarisierten Zivilisation, in der wir leben, und in der Schule und auch beim Zusammenkommen mit anderen Menschen, kommen bei uns immer mehr Ideale hinzu, nicht nur in der Familie ...
Du weißt, ’Sei zufrieden, sei froh, sei tüchtig’ usw.
Das Ideal ist, wir sollen schnell, effektiv, rationell und vor allem bequem sein für andere und gerne auch für uns selbst, und wir sollen das mit der Anpassung gut hinkriegen, so, dass wir nicht eine Zumutung sind.
Wir dürfen andere nicht warten lassen.
Sei rechtzeitig da, sonst müssen sie warten, und das ist schlecht.
"Sowas wird bestraft" = dann darfst du nicht dabei sein.
Anforderungen, die als Erwartungen da sind, und die die Bedingungen darstellen, ob man dich gern haben soll oder nicht ... anstatt Person und Tat auseinanderzuhalten.

Wenn wir durch BEELTERUNG beim Heranziehen von Liebe zum Fühlen frei von Bedingungen sind, ’jene Person macht was sie kann, und es ist nicht besser als das, na gut’ ... hinreichend gut ... dann weiß ich das, aber ... die Person ist ja nicht eine Handlung, ein Objekt, es gibt sicherlich tausend Gründe dafür, wie sie tut, Konstitution, Erziehung, kennt die Uhr nicht usw ... ja, ja, ist nichts passiert, wir werden ja überleben.
Niemand kann einem anderen Menschen die Zeit wegnehmen, wenn du wartest, du bist da ... du lebst ... du kannst an etwas denken, dich amüsieren, indem du etwas anguckst, etwas lauschst, mit jemandem redest ... die Zeit gehört dir und du bestimmst, was sie enthält.
Wenn du die Zeit mit Frustriert-Sein und Irritiert-Sein verbringst, weil eine andere Person nicht pünktlich da ist ... jaa ... dann benutzt du deine Zeit so ... sei dann zufrieden damit, dass du unzufrieden bist.

Vergiss es sobald du mit der Person zusammen bist und siehe ein, dass es nur deine Phantomangst war, die sich in diese Bewertung gegossen hat und sie war nicht die Wirklichkeit, so, jetzt ist die Person da und wir können es gemütlich miteinander haben.
Gib der Bewertung Flügel und lasse sie fliegen und davonfahren.

<div class="handwritten">
Und wenn es nicht geht, sie fliegen und fahren zu lassen, was tue ich dann?
</div>

Ausatmen, spüre die Freude, die Person ist endlich gekommen, genieße die Geborgenheit, die von Innen kommt, dass nichts passiert ist, lache, rede, nimm einander in die Arme oder tue was auch immer, um Raum zu bekommen, das was gewesen ist, sich auflösen zu lassen.

Was wir tun können, ist gerade unsere Bewertungen zu ’waschen’, wie bei meinem Beispiel, und umdenken.
Das braucht Zeit, es gibt keinen geraden Weg, und was man vom Körper mitkriegt, sind oftmals Orakelantworten und wir brauchen einen langen Zeitraum, wo wir sie bei uns tragen, mit ihnen ringen, in Gedanken bei ihnen sind, gemeinsam mit anderen immer wieder über sie rätseln, nach ihrem Sinn fragen, und Erlebnisse haben, wo es uns einleuchtet.
Was uns nicht nützt, ist unsere eigene Enttäuschung am Anderen, weil er zu spät kam, rauszulassen.
Das führt nur dazu, dass wir einen Bumerang-Effekt bekommen und vertieft unsere eigenen Bewertungsempfindungen.
Dann bleibt es viel länger bei uns und zerstört die Zeit, die wir miteinander in Freude hätten verbringen können.
Aber oft tun wir dies statt des anderen, und dann sind wir zu zweit darin, eine gemeinsame unglückliche Zeit zu haben, anstatt dass das Unvollständige zum Abnehmen des Irrgefühls führt.

Wenn uns das Anschauen von einem anderen Standort aus, einige Male gelingt, so werden wir so viel lichter und froher im Sinn innen und sooo viel gemütlicher anderen Menschen gegenüber, bei denen wir Fehler und Mängel entdecken.
Das ist eine schöne Erfahrung.

Es ist verantwortlich leben, wenn es um unsere eigenen unbekannten Ängste geht, und wenn wir es tun, so werden wir uns frei fühlen.
Dies ist die einzige Freiheit, die des Namens wert ist, diese Empfindung nicht durch das Übernehmen der Verantwortung belastet zu sein, weil es nicht geht, da Verantwortung ein Zustand ist, kein Produkt, das man nehmen kann.
Wenn wir Verantwortung zu einem Produkt machen, so fordert dieses Produkt Anpassung, und da werden wir unfrei.
Die Leute sind dabei heutzutage zu behaupten, andere sollen Verantwortung übernehmen und dann dem angepasst sein, unfrei und mit einem Mühlstein um den Hals beladen ... bringt viel Irrgefühl, viel Meiden und Entkommen, und bringen alle zur Verfügung stehende Mittel hervor, um diese Produktverantwortung zu meiden, die man sowieso nicht übernehmen kann.
