## Mit Gewalt-Traumata umgehen

[//]: # "Frage: Wie hilfst du Kindern, die der Gewalt von Erwachsenen ausgesetzt waren, die den Kindern Angst eingejagt hat, sodass sie abgeschaltet haben und als Erwachsene sich auch nicht schützen können, sondern, wenn sie wieder in bedrohliche Situationen kommen, wie Kinder reagieren ... Menschen die einen sehr ärmliches Leben bekommen, weil sie ständig Angst haben und die immer wieder in ihren alten Traumata landen?"

<div class="handwritten">
Wie hilfst du Kindern, die dem, was du erzählst, ausgesetzt waren, der Gewalt von Erwachsenen, die den Kindern Angst eingejagt hat, sodass sie abgeschaltet haben, und als Erwachsene sich auch nicht schützen können, sondern wie Kinder bleiben ... Menschen, die einen sehr ärmliches Leben bekommen, weil sie ständig Angst haben und die in ihren alten Traumata immer wieder landen.
Ich rate, du redest über das, was man heute posttraumatischer Stress nennt, oder?
</div>

Ja, viele Diagnosen gehören dazu, aber es sind nur die Symptome, die diagnostiziert werden.
Ein knappes Tausendstel ungefähr, ist konstitutionell, wir haben etwa 10% schwache Stellen in unserer Konstitution und es kann eine solche schwache Stelle sein, die latent daliegt und von dem einen oder anderen unglücklichen Umstand ausgelöst wird ... aber meistens sind es Symptome, dass wir irgendwie irgendwann während der ersten drei Jahren im Leben existenziell erschrocken sind, und da sind wir nicht reif, dies zu verarbeiten.
Frühestens mit etwa 15 Jahren sind wir reif und erfahren genug, um überhaupt einmal in so einer Weise zu denken, die ich primär nennen würde.
Dann spielt es eine entscheidende Rolle, ob wir mit jemandem zusammenleben, der ständig diese Wiederkehr zur frühen Kindheit auslöst, zum Beispiel mit einem der Elternteile.
Dann geht es überhaupt nicht zu lösen, wenn man zu Hause wohnt, erst wenn man weggezogen und selbständig und finanziell unabhängig ist.
Man kann den Ast nicht absägen, auf dem man selber sitzt, denn da ’stirbt’ man und das kommt einem lebensbedrohlich vor.
Als Erwachsener autonom, freistehend von der Kindheit sind ganz andere Möglichkeiten da.

[//]: # "Frage: Wenn mir jemand begegnet, der ein existenzielles Gewalt-Trauma hat und dort immer wieder stecken bleibt, oder wenn ich selber solche Schwierigkeiten in meinem Leben bekomme, was brauche ich da, und was ist dabei wichtig zu wissen, um andere unterstützen zu können?"

<div class="handwritten">
Wenn mir jemand mit diesen Schwierigkeiten begegnet oder wenn ich selber solche Schwierigkeiten in meinem Leben bekomme, was brauche ich da, und was ist dabei wichtig zu wissen, um andere unterstützen zu können?
</div>

Erst einmal geht es darum, dich um deine eigenen Ängste zu kümmern.
Wenn jemand ein existenzielles primäres Trauma hat, das sich aktualisiert, so löst das Irrgefühl einen Reflex bei dir aus, diese Person in Ruhe zu lassen, auf Abstand zu gehen, dich an ihr nicht zu stören usw.
Das ist ein Überbleibsel aus der Kindheit der Menschheit, wo es um ein wildes Raubtier ging, dem man zu nahe kam.
Da sollte es so sein, dass das Raubtier verwirrt werden würde, und seinen Appetit verlieren und den Menschen nicht als Beute sehen sollte, sondern stattdessen als ein Stinktier, das man lieber meiden sollte.

Dieser Duft des Irrgefühls, der abgesondert wird, nehmen wir Menschen nicht wahr, aber wir reagieren in der gleichen Weise darauf, wie die Raubtiere es getan haben und dessen brauchen wir uns bewusst zu werden und uns darüber hinüberwegsetzen, einfach ignorieren, und trotzdem in Kontakt gehen.
Gerade das Einsehen, dass die Person nicht in Lebensgefahr ist, und dass ich selber kein Raubtier bin ... wenn ich dies blitzschnell denken kann, so lässt oft der Duft des Irrgefühls beim anderen nach und dadurch auch mein verwirrter Auf-Abstand-gehen-Mechanismus.

Was dieser Mensch von dir braucht, ist nur, dass du im Moment hier anwesend bist, und dass du bei ihm die äußere Fürsorge bist, und du für die vernünftigen Impulse da bist.
Das heißt, du sagst wann es Zeit zum Essen ist und du sorgst dafür, wann es Zeit zum Schlafen ist, und du sorgst dafür, wann es Zeit zum Aufstehen ist.
Du sorgst für die äußere Struktur, damit die Person, die in ihrer Schock-Trauma-Auflösung ist, die äußere Geborgenheit bekommt, die der inneren Geborgenheit bei dieser Person großen Platz macht.
Die Person darf so chaotisch sein, wie sie ist, und plappern, quasseln, weinen, schreien, knurren ... und alle andere Ausdrücke, die kommen ... du schaust nur, dass die Schäden nicht zu arg auf diese Person zurückschlagen.

Wenn du etwas gut kannst, womit du zufrieden bist, so tue das.
Zum Beispiel wenn du gerne Gitarre spielst, singst, bäckst, laut vorliest, hochwertige Filme anschaust usw.
Gerade etwas machen, mit dem du vorher zufrieden bist und weißt du tust es gerne und hast den anderen dabei.
Je mehr die Person darin bleiben kann, desto größer ist die Wahrscheinlichkeit, dass es sich entlädt, dass die Person zur Verarbeitung kommt und mit der Zeit zur Neuorientierung.
Die Person fängt mit dieser Erfahrung von vorne an, und wächst zum Hier-Jetzt-Funktionieren auf diesem Gebiet auf.
Das meint man, wenn man vom Geheiltwerden aus dem eigenen Inneren spricht.

Es bist nicht du, die etwas anderes machst, außer du selber sein im Sein hier und jetzt und die andere Person dabei haben.

<div class="handwritten">
Ja, das schaffe ich wohl schon, wenn ich nicht dasselbe PTS habe wie diese Person ... dann geht es wohl nicht, dann ziehen wir einander nur mehr und mehr in Stress?
</div>

Ja, bis einer von euch das Komische darin sieht, dass ein Irrgefühl nicht gefährlich oder tödlich ist, dann kann es amüsant und interessant werden und die Krise kann zur Entwicklung führen.
Eine schöne Beschreibung der Krisenentwicklung ist das Buch von Johan Kullberg, "Krise und Entwicklung".
Es ist etwas psychoanalytischer als wie ich es darstelle, aber trotzdem ganz in Ordnung.

Erinnere dich, dass wir einzigartige Individuen sind, die auf unsere eigene einzigartige Weise reagieren und wir entkommen unseren Traumata auf unsere ganz eigene besondere einzigartige Weise.
Damit meine ich, die Lösung kann nicht von außen kommen, sondern es bin nur ich selber, der Aha-Erlebnisse bekommt, die die Veränderung möglich machen.
Zum Beispiel, eine Person findet heraus, dass sie eine Entscheidung traf, die Mutter zu hassen, weil sie so blöd war und ihn daran gehindert hat den Blumentopf zu zerschlagen.
"Ich hasse, ich hasse, ich hasse ..." und so schmoll die Person bis es vorbei war, aber der Hass blieb noch stecken.
Die Beziehung zur Mutter wurde zur Distanz.
Die Entladung verlor sich, weil es bei der Entladung um etwas ganz anderes ging, vielleicht, dass das Kind von seinem Stiefvater gemein behandelt worden war, aber hatte Angst sich bei ihm abzureagieren und da geschah sie bei der Mutter stattdessen und mit der Zeit vergaß der Körper warum diese Hassgefühle entstanden waren und dann konvertieren sie zu Irrgefühl.
Die Person wird als Erwachsener gehemmt und begrenzt.
Wird von innen bei seiner Arbeit und in der Beziehung mit seinem Partner gestoppt usw.
Vielleicht wiederholt er Verhaltensweisen bei den eigenen Kindern ... das kann sehr unterschiedlich zum Ausdruck kommen.
Mit einem Aha-Erlebnis von dieser Erinnerung kann eine neue Entladung einhergehen, und damit wird die Gestalt rund und verblasst und erlöscht.
