## Was ist Irrgefühl?

[//]: # (Frage: Was ist Irrgefühl (Schwedisch: Ångest)?)

Was ist Irrgefühl?
Die allerernsteste Angst, an die wir gar nicht zu denken wagen, ohne dass dies gleich in Irrgefühl umschlägt, ist das, was man Todesangst nennt.

Als Kinder wissen wir noch nicht, was Tod heißt, dass es das Ende ist vom Hier-und-jetzt-Leben, dass wir dann in die gleiche Lage zurückkehren, in der wir waren, bevor wir entstanden sind.
Wir teilen uns in das Materielle und das Immaterielle auf.
Der Körper geht zur Erde zurück und wird dort in Erde umgesetzt.
Das Immaterielle, das, was ohne Substanz ist, geht zu dem Dunklen im Universum zurück und wird zur Information, von Nutzen von allem Lebendigen.

Während der Entwicklung zum Erwachsenen reift die Fähigkeit heran, diese Tatsache verstehen zu können und ... wenn wir dann aus irgendeinem Grund in Lebensgefahr gewesen sind, so kann diese Erinnerung klar wie Kristall werden und Angst auslösen, die wir entladen bräuchten, aber leider ... oft ... wird diese Angst verdrängt und der Schock, den wir bekamen als dies geschah, wird zu einem Trauma.
Und wir werden versuchen, die Nähe dieses Traumas zu meiden ... meistens unser ganzes Leben lang ... wenn wir nicht irgendwo unterwegs den Mut bekommen, in diesem Irrgefühl drinzubleiben, den Schmerz zu erleben, diese Angst zu entladen, bis sie dem Körper erlöscht und ein Chaos hinterlässt ... wenn wir in diesem verbleiben ... geht dieses Chaos in einen Leerraum über, wo es möglich ist, diesem Schweren Erfahrung zu entziehen und bereichert zu werden mit einem Erleben der Freiheit, die von dem Äußeren unabhängig ist.
Dann wird die Unvollständigkeit in dem Bewusstsein dieser gemachten Erfahrung, die wir bei dieser einen Gelegenheit bekamen, bereichend.
Die schlimmste Folge des Irrgefühls vor dem Tod ist, dass wir es nicht wagen, das Leben lebendig zu leben und voll des Spielens und froh zu sein, denn dann vergegenwärtigt sich, dass wir sterblich sind und das ist es, was wir zu meiden versuchen, um dadurch nicht die Todesangst zu spüren.

Das hemmt und begrenzt unser Leben auf eine Weise, die Gleichgültigkeit anstatt Gefühle schafft - eine Form von Stillstand - und das erhöht das Bedürfnis nach Gefühlen und bekommt oft die Diagnose der Depression.

Was ist ein Irrgefühl?
Die meisten haben bei sich so ein Gefühl erlebt, dass etwas dem Rücken entlangkriecht, ein Unbehagen, es ist so als würden wir im Nacken Gänsehaut bekommen.
Es wird so erlebt wie wenn es eine Phantomgestalt im Dunkeln im Innern gibt, und manchmal auch da draußen, die einem übel will und die angreift und mit Todesgefahr droht.
Etwas, dem man entkommen und es loswerden möchte, am liebsten ausmerzen und vergessen, dass man je solches Fühlen hatte.
Ein Zustand voller Schmerz.

<div class="handwritten">
Jaa, so fühlt sich das an und das kann man auch in tausend anderen Arten spüren, schlimmer noch oder weniger schlimm, ich verstehe nicht, warum wir Menschen damit leben und darunter leiden müssen? Ich sehe darin keinen Sinn ...?
</div>

Weißt du denn, was Irrgefühle sind?

<div class="handwritten">
Eigentlich nicht, aber ich habe von dir gehört, dass ihr auf Schwedisch ein Wort habt, das Ångest heißt.
Meinst du dieses Wort?
</div>

Ja, aber auf deutsch gibt es das Wort so nicht und deshalb muss das Wort Irrgefühl dafür herhalten.
Wenn du einfach unserem Gespräch folgst, wird dir vermutlich klar werden, was mit dem Wort gemeint ist.
Ein Irrgefühl ist ein amöbenähnliches Gefühl.
Es entsteht, wenn der Körper sich nicht mehr daran erinnert, weshalb er die Grundgefühle geschaffen hat ...
Er schafft Gefühle, um Veränderungen in unserem Innern möglich zu machen ...
Aber wenn wir nicht dem gemäß leben, so werden diese geschaffenen Gefühle in den Zellen gespeichert und wenn der Körper sich nicht mehr an sie erinnert, so werden sie zu Irrgefühlen.

Das, was wir gefühlsmäßiges Reagieren nennen, das ist, dass etwas passiert, was bewirkt, dass der Körper diese Gefühle schafft, zum Beispiel wir verlieren etwas, das uns lieb und wertvoll ist.
Um dies zu ertragen brauchen wir einen Übergang, von dem, dass wir etwas in einer bestimmten Weise hatten, zu dem, dass wir dasselbe verloren haben.
Erst wird der Körper niedergeschlagen.
Die Gedanken und die Zeit halten an, es wird ganz unreal, dann kommt die Reaktion, wir zucken irgendwie kurz und dann kommen die Tränen, die Wut, die Angst, die Trauer ...
Der Körper entlädt etwas, damit ein Umdenken möglich wird und geschehen kann.
Wenn wir ohne dieses einfach weitermachen, so wird die Entladung nicht zu dem, was sie werden sollte, und stattdessen vergiften diese Gefühle uns im Innern, sie bleiben als Müll in unseren Zellen zurück.

Diese Grundgefühle können als Wellen über lange Zeit hin immer wieder kommen oder sie können als ein Zusammenbrechen kommen, so dass wir beim Entladen bleiben, bis wir auf der Innenseite leer werden.

Diese Leere ist ein Vakuum, und gleichzeitig ist sie ein Chaos, eine Form von Unbestimmtem, nichts ist mehr wie es war, und es ist nicht möglich weiter zu machen, wie wenn dies nie geschehen wäre ...
Und dennoch geht das Leben aufgrund dieser Gefühlsreaktion weiter.
Nach der Entladung entsteht dann innen ein leerer Raum.
Das ist der Rest von dem, was früher war und wenn wir in ihm bleiben, so wird er von unten her erfüllt, von innen, von unserer eigenen Erfahrung und es atmet, und da entsteht die Möglichkeit, anders zu denken, neu zu denken, sich zu verändern ...
Das Leben geht weiter, aber es ist traurig, dass wir ein solches Ideal haben, dass es negativ ist, Gefühle zu entladen und uns der Wirklichkeit nach zu verändern ...
Dass wir darin stur bleiben, dass es so nicht sein soll, sondern, dass es so sein soll, wie wir es haben wollen, und das ist schon Geschichte, da die Veränderungen schon geschehen sind.

Wir können jegliches Gefühl verdrängen, um was es sich auch handeln mag, und wir werden Gehirngespenster bekommen und Irrgefühl, wenn wir mit ihnen nicht fertig werden, mit ihnen irgendwie leben.
