## Kindheitstrauma - Eine Fallbesprechung

<div class="handwritten">
Wieder kriege ich etwas zum Überlegen und das ist gut.
Irgendwie ist das Irrgefühl während wir geredet haben viel weniger furchterregend geworden ... und ganz plötzlich denke ich nicht an Irrgefühl als umgewandelte Gefühle von einem tatsächlichen Geschehen.
Ich werde mich auch geborgener fühlen, wenn ich Personen mit Irrgefühl treffe, denn ich weiß, dass Irrgefühl an sich harmlos ist, und dass ich das nicht verändern kann.
Dass Leute im Irrgefühl sein dürfen bis sie mit sich selber in Kontakt kommen innen und der Körper und das Bewusstsein zusammenarbeiten können, um es zu lösen.
Dass ich einfach weiterhin mit ihnen zusammenkomme, ob sie nun Irrgefühl haben oder nicht.
Schön zu wissen, dass es geht, dass ich das ignoriere und letztenendes nur ich selber sein brauche.

Gibt es einen Fall, den du mir beschreiben kannst?

</div>

Ja, ich habe einen dampfend heißen aktuellen Fall, bei dem ich Supervision gebe und ich kann (für den Datenschutz zurechtgemacht) unser Gespräch damit abrunden.

Die Person, bei der ich Supervision mache, erzählt von einem Brief, den ihm ein Freund geschrieben hat, dem er über lange Zeit hin eine Hilfe war, indem er für ihn da war.

"Ein Freund von mir, der im Moment in den USA bei seiner Mutter ist, hat mir geschrieben, er komme vielleicht in zehn Tagen oder so was mit dem Flieger wieder zurück und da möchte er mich sehen und möchte, dass ich ihm zuhöre und dann verstehe, was wirklich Sache ist.
Von dem er behauptet, es gäbe es in der Realität und ich sollte verstehen, wie schlimm es sei ...

Er spricht von Dämonen und vom Teufel, die sich selber in ihm darleben und auch außerhalb erscheinen und ihm und anderen Hässlichgesichter geben, die sie zu dummen Handlungen veranlassen.

Eine Therapeutin, die er in den USA kennengelernt hat, sagte, er sei als Kind misshandelt worden und dass es dies ist, was sich so ausdrückt, weil er den Menschen, die es getan haben, dies nicht vorwerfen möchte, jemandem von den ihm Nahestehenden, der es gewesen ist.

Er glaubt nicht daran, er weiß es nicht ... aber das fühlt sich bei ihm nicht stimmig an, und er glaubt es nicht.
Ihm ist das eine von vielen Theorien, und ihm sind etliche von verschiedenen Personen vorgelegt worden, da er schon lange so dran ist.
Er sagt, er war dreimal bei ihr und es war nahe daran, dass sein Leben aufgeklärt worden wäre und er wäre dadurch zu der Zeit geheilt geworden, aber da geschah stattdessen ihre Diagnostizierung und unterbrach das Ganze und er war sich seiner nicht so sicher und blieb nicht in der Verfassung, wo die Aufklärung war und daher wurde es wieder wie es war ... dieses, der "kranke kleine uninteressante Junge" zu sein - sagt er, der er in den Augen der anderen ist.

Er sagt, er verstand sie alle, muss aber seine eigenen Wege finden, das auszudrücken, und das ist nicht möglich, wenn jemand diagnostiziert oder Ratschläge gibt.
Seine Art sei viel geistiger als das und das will niemand hören oder verstehen, sagt er.

Er steht immer noch unter dem Einfluss der ganzen Drogen, die er vom Arzt zu Hause verschrieben bekommen hat, und von anderen Drogen, die er nimmt, und er hat Angst, ganz verrückt zu werden, wenn er sich nicht mit ihnen betäubt.

Ohne Medikamente und ohne das andere hält er es nicht aus, sagt er, hält den Schmerz in der Dunkelheit nicht aus.
Das ist unvorstellbar grauenhaft, und dem würde er physische Folter vorziehen, sagt er.

Er sagt auch, dass seine "Seele" weg ist, er hat alles verloren, er hat keinen Anschluss an irgendetwas, er schwebt im Vakuum ... aber eine böse Kraft hat von ihm Besitz ergriffen.

Er glaubt nicht, dass Selbstmord ihn vor der Hölle bewahren wird.
Er glaubt, dies würde vielleicht noch schlimmer und er habe die Macht, dem widerstehen zu können, verloren, und da bleibt er lieber in einem physischen Körper.
Er möchte auch nicht hinter Gitter wie ein Psychopath und deshalb wird er stattdessen steif und unbeweglich und denkt nicht an andere oder daran anderen zu schaden, sowie er auch nicht den eigenen Impulsen folgt, weil sie schädlich sind, das weiß er.

Er weiß nicht, warum sie schädlich sind.
Er kann nichts von all diesem widerstehen und er redet sehr linear, logisch, auch wenn er nicht versteht, und er reflektiert sich selbst in einer besonderen und vernünftigen Weise.

Er sagt auch, er sei böse mit mir, wegen dem, was ich getan habe und dass ich das tat, was ich tat, weil ich nicht geglaubt habe an sein wahres geistiges Erleben, dass ich dem gegenüber skeptisch war, sodass er selber skeptisch wurde und chaotische Dinge getan hat, die von der Umgebung nicht verstanden werden konnten, sondern er kam ins Krankenhaus und wurde zwangsweise eingesperrt.

Er trauert seinem ganzen menschlichen Potential nach, dem dass er nicht fähig war, und dass er nicht so leben kann, dass alle zufrieden sind und sich sicher fühlen, er werde sich das Leben nicht nehmen.
Die brauchen keine Angst zu haben, er ist sich gewiss, sich so einzufrieren zu können, dass er keinem Schaden tut.

Er sagt auch es sei nicht immer er selber, der redet, sondern es ist der Dämon, und er versteht, dass man dem Dämon nicht trauen kann, aber er sagt, es ist dennoch möglich, ihm zu vertrauen, aus dem, was er von sich selber sagt.
Das fühlt sich bei den anderen schief an, aber ihm ist das so, usw. usw.

Er fragt, ob ich ihn anrufen möchte.
Ich habe ihn wirklich gern, und ich rede gerne mit ihm, höre ihm auch zu, aber Skepsis kommt in mir auf, wenn er vom Dämon besessen wird ... aber ich möchte nicht über all seine Dinge mit ihm sprechen, ich stelle ihm keine Fragen zum Geistigen und ich mache mir große Sorge, dass ich bei einem Menschen, der so eine selbstzerstörerische Art hat, was Falsches sage, so dass er letzten Endes sich doch das Leben nimmt und sich selber Schaden zufügt.

Hältst du es für wahrscheinlich, dass er dies tun wird?
’Siehst’ du in etwa wie die Aussichten bei ihm sind, ob er das Überleben hinkriegen wird?
Oder klingt das, was er sagt und tut viel gefährlicher als es in der Wirklichkeit ist?
Wie kann ich ihm helfen?
Was hältst du von seiner Erzählung und von seiner Denkweise und von der Art wie er redet in Bezug auf ihn selber und auf sein geistiges Inneres?
Wie kann ich ihm das Verständnis beibringen, dass es ein magisches Denken ist und nicht die Wirklichkeit?

Die Antwort:

Vor allem, er leidet unter einem schweren Irrgefühl und dieses hat seinen Ursprung darin, dass ihm die Referenz in ihm selber fehlt.
Er hat die Erdung, die Orientierung und die Verankerung in seinem Leben verloren ... dieses Leben im Leben und dass dies das Leben ist und dass er am Leben ist.
Ihm ist das Gefühl, dass es ihn gibt, abhanden gekommen und da gerät er außerhalb seiner selbst in seinem Körper.
Diese Auftrennung ist schreckenerregend und er bleibt dann im Immateriellen, was er das Geistige nennt, damit er dadurch eine Geborgenheit bekommt, dass dies die einzige wirkliche Wirklichkeit ist, die er im Moment erleben kann.

Er geht hin und her zwischen dem Aufleben des Lichts in der Dunkelheit und dem Weggehen des Lichtes ... dann ist es dunkel, schwarz, chaotisch und schwer.
Wenn ihm dieses Licht fehlt, so kommt Angst auf, die er nicht spürt, und in dieser Angst entstehen Dämonen, wie er das Böse nennt.
Wenn es hell ist, so gibt es diese Dämonen nicht, dieses Böse, da wird es beinahe wie normal und da fühlt er, dass er sich selber ist und denken kann, sich verständlich ausdrücken, aber er nimmt deinen Zweifel auf und da verliert er seine Erdung und landet in seinem Vakuum.

Es bist keineswegs du, der diese Situationen herbeiführt.
Ich erinnere mich, dass er in Bezug auf seinen Vater ganz außer sich war, der die Erwartung hatte, er würde sekundär normal funktionieren.
Ich erinnere mich, dass er in ein Chaos geriet, als die Eltern sich scheiden ließen und die Mutter mit einem Mann in die USA zog ... oder?

Was auch immer es war, was sein magisches Denken ausgelöst hat, so bleibt dieses in der Form, die es erhalten hat, als er in Schock war und um sein Leben gefürchtet hat.
Sein Autopilot wurde mit dem ganzen Müll programmiert, der aus dem Dunkeln hochsteigt und vor dem er Angst hat, und er kommt hoch, um ihn vor dem Entladen zu hindern, und offen und verletzlich zu werden und umdenken zu können.
Das Hindernis ist da, um ihn zu schützen, wenn ihm die äußere Geborgenheit fehlt, die es ihm möglich macht, in der inneren Geborgenheit sein zu können.

Der Grund ist, dass er im Schock Reaktionen braucht, in ihnen leben und sie entladen braucht bis es leer und ohne Sinn, chaotisch wird, wie es im Dunkeln des Universums ist, alles gleichzeitig und deshalb unfassbar.

Wenn er imstande wäre, es so sein zu lassen, wie es ist und einfach wartet, so würde es ausebben und leer und chaotisch werden, aber ohne Aufhängung, und da kehrt es sich in ihm um und wird licht und dann kann er wieder normal realistisch denken.

Wenn er aber da an diese Dämonen, die aus der Angst entstehen, glaubt, so behält er den Schrecken und kann nicht aus ihm herauskommen.
Der Grund ist, weil er meint, dass diese natürliche chaotische Leere lebensgefährlich und destruktiv sei, nicht dass sie einfach dazu da ist, die Angst aufzulösen, damit ein Umdenken möglich ist, so dass es wieder hell wird.

Es ist für ihn leider nicht möglich, umzudenken, bis er darin stehen bleibt und es dieser geordnete Leerraum werden darf, wo die lichten Gedanken, Erinnerungen, Erlebnisse, Ahnungen, sich neu ausdrücken oder durch Nostalgie wieder zurückkehren.
Wiederanknüpfung an das primäre Ursprüngliche, und wenn er den Weg dorthin verloren hat, so ist das etwas, bei dem er Hilfe braucht, und gerade sowas ist es, dass es dich als die äußere Geborgenheit gibt.

Wenn du dies verstehst und verstehst, dass es nur von seinem Innern kommen kann, wenn du dein Bedürfnis, tüchtig und aus den Reihen der Guten zu sein, fallen lässt, und einfach bei ihm bist, dort bist, alles magische mystische Dämonische, was aus ihm rauskommt ignorierst, hörst mit Interesse zu, aber sagst nichts, richtest dein Blick auf das Gesunde, schaust, dass er etwas zum Essen bekommt, schaust, dass es in der Küche sauber wird, dass er ein schön gemachtes Bett bekommt, dass er saubere Kleidung bekommt.
Du beschäftigst dich mit dem Notwendigen, wenn er in seinem Chaos ist ... so kann das im besten Fall dazu führen, dass er aus seiner Hölle herauskommt.

Lass ihn seine Medikamente nehmen, solange er diese als äußere Geborgenheit hat.
Das ist wie ein Trostnuckel von einem Kind, das ungern seinen Daumen lutscht.
Am liebsten Placebomedizin, die dem Körper nicht schadet, die aber mit seinem kommunikativen Inhalt doch hilft.

Bitte ihn, diese Medizin gegen anthroposophische Medizin auswechseln zu dürfen, und erzähle ihm, dass diese dem Körper heilen hilft, aber dass es lange Zeit braucht, und dass diese Medizin nichts kaputt macht, sondern nur dem Körper hilft sich zu kurieren.
Dass er sich darauf verlassen und vielleicht mit der Zeit ihr trauen kann.
Sonst braucht er nichts von dir, jedenfalls nicht im Moment, aber schreib mir gerne weiterhin, was dich bedrückt, wenn du ihn siehst, und er wieder nach Hause kommt.

(Ende der Fallbeschreibung)

Dies ist eine Art von Irrgefühl, dem oft eine konstitutionelle schwache Stelle zu Grunde liegt.
Diese schwachen Stellen liegen latent und können das ganze Leben latent bleiben, aber bei jemanden gibt es einfach die Neigung diese Weichstelle unter gewissen Umständen auszulösen.

<div class="handwritten">
Wie weiß man, ob ein Kind diese Neigung hat, und wie kann man es davor schützen, dass diese ausgelöst wird?
</div>

Meistens ist es nicht möglich, das im Voraus zu wissen.
Genausowenig können wir wissen, dass jemand eine größere Geneigtheit hat, beim ersten Mal rauchen süchtig zu werden, während andere jahrelang ab und zu rauchen können, ohne süchtig zu werden und bei einer gewissen Person kommt es nie zur Sucht, auch wenn sie über lange Zeit hin raucht.
Deshalb hat es einen Sinn, Kinder solange wie möglich vor dem Hängenbleiben an Kickgefühlen zu schützen, denn diese Gefühle züchten Sucht, wenn man die Neigung hat.
Vor allem während des Heranwachsens, bevor das Innere des Körpers sich fertig entwickelt hat, kann dieses Kicken dieses Wachstum hindern und das ist schade, denn später im Leben rächt sich das, und man muss von vorn wieder anfangen, um Resistenz zu entwickeln.

Ich meine, es ist gut, alle Kinder auf diese Weise zu schützen.
Dies ist der Grund, weshalb es ein Gesetz gibt, das besagt, dass man ein gewisses Alter inne haben soll, um gewisse Filme anzuschauen, und ein gewisses Alter haben soll, um Sex zu haben, ein gewisses Alter haben soll, um Alkohol zu trinken, und in der Gesellschaft ist der freie Handel mit Rauschmitteln verboten.

Ich meine nicht, dass Verbote gut sind, nur ist der Mensch noch so unentwickelt darin, wenn er erwachsen ist, die Kinder generell zu schützen, und daher sind in unserer Zivilisation gewisse Begrenzungen nötig.
