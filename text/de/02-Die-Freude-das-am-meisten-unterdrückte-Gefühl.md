## Die Freude: das am meisten unterdrückte Gefühl

[//]: # "Frage: Welches Gefühl wird in unserer Zivilisation am msiten verdrängt?"

Weißt du welches Gefühl in unserer Zivilisation am meisten verdrängt wird?

<div class="handwritten">
Vermutlich die Angst, weil sie als Schwäche angesehen wird und dass wir weich und mickrig und abhängig von anderen sind, wenn wir Angst haben ... außerdem tun wir dann lauter dummes Zeug, wir sind nicht rationell und das müssen wir, um von anderen gebilligt zu sein.
Wir werden oft kindisch und reagieren, als wären wir drei Jahre alt oder, und so, dass wir selber den Eindruck haben, das läuft so unheimlich schief.
</div>

Die Angst ist ein wenig besonders, weil sie zwei Seiten hat.
Die eine Seite ist, dass wenn wir nicht geborgen sind, so spüren wir die Angst nicht, sondern gehen da gleich zur Handlung über.
Entweder werfen wir uns vor, nicht gut zu sein, oder wir werfen anderen dasselbe vor, oder wir werden Opfer der Unvollständigkeiten im Leben ... Jedenfalls verstehen wir nicht, dass wir Angst haben, wir reagieren einfach und agieren aggressiv, meistens von der Wut verstärkt.
Dieser Zustand wird oft für Wut gehalten, aber es ist nicht dasselbe.
Was wir da tun, lindert die Angst nicht, sondern verursacht nur mehr Elend, da dies bei uns zum Rückschlag wird ... oder wie ich es zu nennen pflege, wir bekommen davon eine Bumerang-Wirkung ... oft führt das zu etwas, was schlechter ist, leider, es fällt uns schwer in der Wirklichkeit, die sich soeben offenbart hat, zu sein.

Die andere Seite der Angst ist, wenn wir in Zufriedenheit in uns sind, wenn wir geborgen sind, dann kommt eine Direktentladung der Angst und wir reagieren, indem wir kurz zittern, uns friert, schaudert, es schüttelt uns vor Schrecken ... dann entlädt sich dieses Gefühl und wir können sehen, dass es ungefährlich war, nicht zum Sterben, und so ist es aus der Welt.
Dem folgen oft Trauer, Erleichterung, wir kochen vor Wut und so weiter, und das ist rein gefühlsmäßig und entlädt sich von selbst bis der Leerraum da ist und das Neudenken kommen kann.

Aber nein, dies ist nicht das Gefühl, das am meisten verdrängt wird, sondern das ist ... jetzt wirst du staunen ... die Freude.
Es ist das Gefühl von Freude, was am meisten verdrängt wird, sogar dermaßen, dass wir staunen, wenn wir dies bei uns entdecken.

<div class="handwritten">
Du scherzt wohl ... das kann nicht wahr sein ... die Freude ist ja so ungefährlich.
Warum um Himmels Willen würden wir diese verdrängen?
Wie kann das sein ... es ist ja die Freude, nach der wir uns die ganze Zeit sehnen.
</div>

Ja, das stimmt, es ist die Freude, die wir wollen, aber es gibt einen Kontext in unserer Kultur, der der Meinung ist, dass es Sünde ist, froh zu sein.
Wenn wir zu freudig sind, so lassen wir Gott im Stich, dann sind wir egoistisch oder hysterisch oder ketzerisch ... hedonistisch ... und dann geben wir uns den Todsünden hin, zum Beispiel der Schlemmerei und dann sind wir wahrlich keine guten Menschen, die in den Himmel kommen, es sei denn wir tun Buße und bessern uns.

<div class="handwritten">
Aber wir sind ja moderne Menschen und wir sind nicht mehr religiös.
Es sind ja viele, die überhaupt nicht an einen Gott glauben und die kein religiöses Leben mehr führen.
So, warum würden wir dennoch glauben, dass es Sünde ist froh und zufrieden zu sein?
</div>

Als Kontext gibt es dies noch und ist von Politikern unbewusst aufgeschnappt.
Irgendwann in den 50er Jahren hatten wir in Schweden alle demokratischen Ziele erreicht.
Niemand musste frieren, hungern oder ohne Bleibe sein.
Die Arbeitslosigkeit war niedrig und das Wachstum in der Ökonomie war gut, und dadurch hatten alle ein gewisses Maß an Luxus.
Die politischen Richtlinien für die Landwirtschaft in Schweden waren günstig und wir konnten uns selber versorgen, wenn wir es wollten.
Das Verteidigungswesen war aufgebaut und die Demokratie funktionierte mit mehreren Parteien gut.
Die Denkart der Sozialdemokratie war vorherrschend.

Wir bauten unsere politische Situation nicht auf Gegensätze auf, auf Feinden, sondern wir bauten eine Zusammenarbeit.
Es zeigte sich darin, dass Russland uns bewunderte und wir die USA bewunderten. Die Schule war ausgebaut worden und umfasste neun Schuljahre und wir befanden uns in einem inneren Reichtum, der uns half, sekundär, in Alternativen und wahlweise zu denken und auf diese Weise konnten wir das beste aller Leben leben, maßgeschneidert, einzigartig und allgemein.
Die Leute waren guter Dinge mit dankbarer, einsichtsvoller Hoffnung in die Zukunft und die Zufriedenheit war da.
Das Gemeinsame im Land, die Steuereinnahmen, reichten für die Fürsorge um diejenigen, die Außen vor blieben, die süchtig waren, die untauglich für die Industrie waren, die Alten, die Kranken, die Kinder und so weiter.

Das Wohlfahrtssystem kam für ein allgemeines Kindergeld auf und eine Altersrente wurde eingeführt, Krankenrente und Sozialgeld für diejenigen, die nicht fähig waren, in der Gesellschaft zu arbeiten.
Das Volksheim Schweden kümmerte sich auf einer ausgezeichnet sicherstellende Weise um seine Bürger und die Leute waren zufrieden und äußerlich geborgen.
Die Demokratie wurde durch Humanität erschaffen.
Dadurch, dass die Sozialdemokratie all dies erreicht hatte, so hätte es ein paralleles System geben können, zum Beispiel, dass 50 Prozent für das Gemeinschaftliche, als Steuern ... der Rest in die private Tasche.
Die bedingungslose Verteilung der Steuermittel durch den Staat an die Bedürftigen in einer humanen Weise hätte entwickelt werden können, in der gleichen Weise, wie das Kindergeld zustande gekommen war.
Und da die Hälfte des Mehrwerts-Zuwachs-Kapitals in die Gewinne der Unternehmen, sowie in die Löhne der Arbeiter einfloss, so hätte dies eine schöne Balance hervorbringen können ... aber ...

[//]: # "Gedankenfehler: Der Mensch wird faul, wenn er alles bekommt, was er braucht."

Dann hätte es nichts gegeben, wofür man kämpft, und da glaubte man, das mache den Menschen faul, nichtsnutzig und verwöhnt und das durfte nicht geschehen.
Dies ist der größte Gedankenfehler, den man in der modernen Zeit begangen hat, leider, und es ist immer noch ein tragisches Majoritätsmissverständnis, das unkorrigiert geblieben ist.
Das führt zu Irrgefühl wegen der Leistung, führt zu Idealen und zu Erwartungen an uns selber, was mit sich bringt, dass wir massenweise Gefühle aller Art verdrängen und modernes, soziales Irrgefühl schaffen, bei dem wir uns selber nicht genügen und dann glauben wir nicht, dass wir anderen genügen und dann bekommen wir ein Misstrauen uns selber gegenüber, was uns daran hindert, uns selber zu spüren.
Dieses erzeugt bei den meisten Irrgefühl, und dies nennt man heutzutage, dass es den Menschen schlecht geht.

Dieses Irrgefühl tritt oft schon wegen der Leistungsanforderungen in der Schule auf, denen die Schüler sich anzupassen haben, denen die Lehrer sich anpassen, und bei denen sowohl Eltern wie Lehrer von den Schülern fordern, diesen gerecht zu werden ... die Katze auf die Ratte usw, eine Kette im schwedischen Ammenmärchen bis hin zur Anpassungsfalle, die wir heute in Absurdum in der heutigen Gesellschaft leben.

”Liebe Katze, friss die Maus.
Denn die Maus will nicht abnagen den Strick, und der Strick will nicht binden den Metzger, und der Metzger will nicht schlachten den Ochsen, und der Ochse will nicht trinken das Wasser, und das Wasser will nicht löschen das Feuer, und das Feuer will nicht brennen den Reisig, und der Reisig will nicht schlagen den Jungen, und der Junge will nicht in die Schule.”
Der Trend der Unzufriedenheit wurde angenommen.
Niemand sollte zufrieden sein, denn dann ... dann würden alle Opfer der Armut werden, dies war implizit darin.

<div class="handwritten">
<span class="first-words">Ein schwedisches Ammenmärchen:</br /></br /></span>
Es war einmal ein Junge, der schrecklich faul war.
Er wollte nicht in die Schule gehen.
Dann ging die Mutter zum Reissig und sagte:
- Reisig, prügle den Jungen, denn der Junge will nicht zur Schule.
- Nein, sagte der Reisig.
Dann ging die Mutter zum Feuer und sagte:
- Feuer, brenne den Reisig, denn der Reisig will den Jungen nicht schlagen und der Junge will nicht zur Schule.
- Nein, sagte das Feuer.
Da ging die Mutter zum Wasser und sagte:
- Wasser lösche das Feuer, denn das Feuer will den Reisig nicht brennen und der Reisig will den Jungen nicht schlagen und der Junge will nicht zur Schule.
- Nein, sagte das Wasser.
Da ging die Mutter zum Ochsen und sagte:
- Ochs, trinke das Wasser leer, denn das Wasser will das Feuer nicht löschen ... (usw)
- Nein, sagte der Ochsen.
Da ging die Mutter zum Metzger und sagte:
- Metzger, schlachte den Ochsen, denn der Ochse will das Wasser nicht wegtrinken ... (usw)
- Nein, sagte der Metzger.
Da ging die Mutter zum Seil und sagte:
- Seil, erhänge den Metzger, denn der Metzger will den Ochsen nicht schlachten ... (usw)
- Nein, sagte das Seil.
Da ging die Mutter zur Ratte und sagte:
- Maus, zernage den Strick, denn der Strick will den Metzger nicht erhängen ... (usw)
- Nein, sagte die Maus.
Da ging die Mutter zur Katze und sagte:
- Katze, fang die Maus, denn die Maus will den Strick nicht zernagen ... (usw)
- Nein, sagte die Katze.
- Wie ist's, wenn du von mir ein wenig Sahne bekommst?
- Mja, dann mach' ich es wohl schon.<br /><br />
Und die Katze auf die Maus, und die Maus auf den Strick, und der Strick auf den Metzger, und der Metzger auf den Ochsen und der Ochs auf das Wasser und das Wasser auf das Feuer, und das Feuer auf den Reisig und der Reisig auf den Jungen und der Junge ging zur Schule.
</div>

Dann fingen die politischen Winde an in einer bestimmten Richtung zu wehen ... man dürfte sich nicht zufriedengeben, denn dann würde es nicht besser.
Man befürchtete, dass die Menschen dann faul und unnütz werden würden, anstatt ... zufrieden, neugierig, kreativ, operativ und neuschaffend ... damit man vorankommt, muss man unzufrieden sein, sich hohe Ziele vornehmen und kämpfen, um sie zu erreichen.
Also schuf man höhere Ideale, nach denen man sich streckte.
Alles sollte ein Maximum an Gleichberechtigung, Gerechtigkeit und Brüderlichkeit werden und alle Menschen sollten in großem Luxus leben ...
Das war gleichbedeutend mit Glück.
Dieser Fehler ist dabei, die Menschen in dieser Zivilisation zu veröden, indem er so viel Irrgefühl durch die Anpassungsfalle erzeugt.

Als wir keinen Kampf mehr gegen Armut und Analphabetismus führen brauchten, so sollten wir für höhere absolute Ziele kämpfen, am liebsten für solche Ziele, die man nicht erreichen konnte, so dass wir wirklich unzufrieden sein könnten und damit immer pflichtbewusst kämpfen, um es besser zu haben ... uns nicht zufriedenzugeben ... nichts war gut genug.

<div class="handwritten">
Aber ist es denn nicht so? Ich meine, wenn alle zufrieden sind und nichts tun wollen, nichts haben, wofür sie kämpfen wollen, um was besser zu können oder um es besser zu haben, so endet das wohl wieder in Armut, oder? Werden wir uns dann nicht wertlos vorkommen?
</div>

Gefühle der Wertlosigkeit kommen nicht dadurch, dass wir verantwortlich in Freiheit leben, sie kommen dadurch, dass wir der Leistungsforderung nachkommen, besser zu sein, als wir sind, und dass wir ständig, das Gut-genug-sein-wie-wir-sind, herabwerten, und dass wir zufrieden sein können damit, und dass wir weiterhin die Kunst üben können, unsere Talente zu entwickeln.

Probiere es selber, dann bekommst du eigene Erfahrung.
Wenn du zum Beispiel denkst, wenn du kochst: ”Wenn es ums Kochen geht, bin ich nicht der beste der Welt, aber ich bin gut genug und ich koche gerne.
So, es wird lecker genug und damit bin ich zufrieden”.

Wenn niemand bewertet, auch du nicht, dass jemand anders besser sein soll, als er ist, so gibt es keine Faulheit, und dann kann niemand sich selber herabwerten, sondern wird stattdessen zufrieden, indem er gut genug ist.

Es ist fundamental einfach, aber es ist nicht leicht, das Bewerten zu lassen.
Es liegt ein Kick darin, besser zu sein als man ist, eine Süße.
Es ist etwas Momentanes, es verschwindet schnell und deshalb wollen wir nur mehr und mehr und mehr Kicks, und irgendwann wird es unmöglich, und dann kommt die Enttäuschung, und um diesem Schmerz zu entkommen, werten wir uns selber ab ... du weißt ... der Fuchs und die Ebereschenbeeren ... der Schmerz der Enttäuschung wird übergangen.

”Der Fuchs hatte Hunger.
Und es saß eine Krähe in einer Eberesche mit roten Beeren.
Der Fuchs fragte: ’Sind die Beeren lecker?’
Die Krähe antwortete, ’Die sind süß und lecker, kra-kra’.
Der Fuchs machte dann viele Sprünge, um an die Beeren zu kommen, war aber ein schlechter Kletterer, und kam nicht hin ... da sagte der Fuchs: ’Ach, sie sind sowieso gewiss sauer, es kann mir gleich sein’, und so ging er.”

So entsteht ein Gedankenfehler.
Wir machen eine negative Erfahrung, und statt die Enttäuschung und den Schmerz zu ertragen und zu entladen und dies zur Erfahrung werden zu lassen, die wir bei späterem Anlass nutzen können ... so werten wir um, und es wird später zur Anpassung an diesen Gedankenfehler führen und das ist eine Grundlage für Irrgefühl.
